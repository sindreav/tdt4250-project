/**
 * generated by Xtext 2.15.0
 */
package tdt4250.generator;

import com.google.common.base.Objects;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.AbstractGenerator;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import tdt4250.xhtml.Comment;
import tdt4250.xhtml.InnerText;
import tdt4250.xhtml.Variable;
import tdt4250.xhtml.XmlAttribute;
import tdt4250.xhtml.XmlElement;
import tdt4250.xhtml.XmlStyle;

/**
 * Generates code from your model files on save.
 * 
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#code-generation
 */
@SuppressWarnings("all")
public class XhtmlGenerator extends AbstractGenerator {
  private List<String[]> variables = CollectionLiterals.<String[]>newArrayList();
  
  private int whiteSpace = 1;
  
  private String html = "<html>";
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    final String fileName = input.getURI().trimFileExtension().lastSegment();
    EList<EObject> _contents = input.getContents();
    EObject _head = null;
    if (_contents!=null) {
      _head=IterableExtensions.<EObject>head(_contents);
    }
    boolean _tripleEquals = (_head == null);
    if (_tripleEquals) {
      return;
    }
    this.iterate(((Object[])Conversions.unwrapArray(IterableExtensions.<EObject>head(input.getContents()).eContents(), Object.class)));
    this.newLine();
    String _html = this.html;
    this.html = (_html + "</html>");
    fsa.generateFile((fileName + ".html"), this.html);
    this.whiteSpace = 1;
    this.html = "<html>";
  }
  
  public void iterate(final Object[] o) {
    for (final Object c : o) {
      if ((c instanceof XmlElement)) {
        this.generateElement(((XmlElement)c));
      } else {
        if ((c instanceof Comment)) {
          this.generateComment(((Comment)c));
        } else {
          if ((c instanceof Variable)) {
            this.useVariable(((Variable)c));
          } else {
            if ((c instanceof InnerText)) {
              this.generateInnerText(((InnerText)c));
            }
          }
        }
      }
    }
  }
  
  public void generateComment(final Comment c) {
    this.newLine();
    String _text = c.getText();
    int _length = c.getText().length();
    int _minus = (_length - 1);
    String _substring = _text.substring(1, _minus);
    String _plus = ("<!--" + _substring);
    String _plus_1 = (_plus + "-->");
    this.addToHTML(_plus_1);
    this.newLine();
  }
  
  public void generateElement(final XmlElement e) {
    String s = "<";
    String type = "";
    String _sugarType = e.getSugarType();
    boolean _tripleNotEquals = (_sugarType != null);
    if (_tripleNotEquals) {
      String _sugarType_1 = e.getSugarType();
      if (_sugarType_1 != null) {
        switch (_sugarType_1) {
          case "d":
            type = "div";
            break;
          case "s":
            type = "span";
            break;
          case "h":
            type = "head";
            break;
          case "b":
            type = "body";
            break;
          case "sc":
            type = "script";
            break;
          case "t":
            type = "title";
            break;
          default:
            type = "UNKNOWN";
            break;
        }
      } else {
        type = "UNKNOWN";
      }
    } else {
      type = e.getType();
    }
    String _s = s;
    s = (_s + type);
    String _id = e.getId();
    boolean _tripleNotEquals_1 = (_id != null);
    if (_tripleNotEquals_1) {
      String _s_1 = s;
      String _id_1 = e.getId();
      String _plus = (" id=\"" + _id_1);
      String _plus_1 = (_plus + "\"");
      s = (_s_1 + _plus_1);
    }
    String _className = e.getClassName();
    boolean _tripleNotEquals_2 = (_className != null);
    if (_tripleNotEquals_2) {
      String _s_2 = s;
      String _className_1 = e.getClassName();
      String _plus_2 = (" class=\"" + _className_1);
      String _plus_3 = (_plus_2 + "\"");
      s = (_s_2 + _plus_3);
    }
    EList<XmlAttribute> _attributes = e.getAttributes();
    for (final XmlAttribute a : _attributes) {
      String _s_3 = s;
      String _name = a.getName();
      String _plus_4 = (" " + _name);
      String _plus_5 = (_plus_4 + "=");
      String _value = a.getValue();
      String _plus_6 = (_plus_5 + _value);
      s = (_s_3 + _plus_6);
    }
    int _size = e.getStyles().size();
    boolean _tripleNotEquals_3 = (_size != 0);
    if (_tripleNotEquals_3) {
      String _s_4 = s;
      s = (_s_4 + " style=\"");
      EList<XmlStyle> _styles = e.getStyles();
      for (final XmlStyle st : _styles) {
        {
          if ((st.getAttribute().contains("\'") || st.getAttribute().contains("\""))) {
            String _attribute = st.getAttribute();
            int _length = st.getAttribute().length();
            int _minus = (_length - 1);
            st.setAttribute(_attribute.substring(1, _minus));
          }
          if ((st.getValue().contains("\'") || st.getValue().contains("\""))) {
            String _value_1 = st.getValue();
            int _length_1 = st.getValue().length();
            int _minus_1 = (_length_1 - 1);
            st.setValue(_value_1.substring(1, _minus_1));
          }
          String _s_5 = s;
          String _attribute_1 = st.getAttribute();
          String _plus_7 = (_attribute_1 + ":");
          String _value_2 = st.getValue();
          String _plus_8 = (_plus_7 + _value_2);
          String _plus_9 = (_plus_8 + ";");
          s = (_s_5 + _plus_9);
        }
      }
      String _s_5 = s;
      s = (_s_5 + "\"");
    }
    int _size_1 = e.getContents().size();
    boolean _tripleEquals = (_size_1 == 0);
    if (_tripleEquals) {
      this.addToHTML((s + "/>"));
      return;
    }
    String _s_6 = s;
    s = (_s_6 + ">");
    this.newLine();
    this.addToHTML(s);
    this.whiteSpace++;
    this.newLine();
    this.iterate(((Object[])Conversions.unwrapArray(e.getContents(), Object.class)));
    this.whiteSpace--;
    this.newLine();
    this.addToHTML((("</" + type) + ">"));
    this.newLine();
  }
  
  public String generateInnerText(final InnerText i) {
    return this.addToHTML(i.getText());
  }
  
  public Object useVariable(final Variable v) {
    Object _xblockexpression = null;
    {
      int index = (-1);
      for (int i = 0; (i < this.variables.size()); i++) {
        String _get = this.variables.get(i)[0];
        String _name = v.getName();
        boolean _equals = Objects.equal(_get, _name);
        if (_equals) {
          index = i;
        }
      }
      Object _xifexpression = null;
      if (((index == (-1)) && (v.getValue() != null))) {
        String _name = v.getName();
        String _value = v.getValue();
        _xifexpression = Boolean.valueOf(this.variables.add(new String[] { _name, _value }));
      } else {
        String _xifexpression_1 = null;
        if (((index != (-1)) && (v.getValue() == null))) {
          _xifexpression_1 = this.addToHTML(this.variables.get(index)[1]);
        } else {
          String _xifexpression_2 = null;
          if (((index != (-1)) && (v.getValue() != null))) {
            _xifexpression_2 = this.variables.get(index)[1] = v.getValue();
          }
          _xifexpression_1 = _xifexpression_2;
        }
        _xifexpression = _xifexpression_1;
      }
      _xblockexpression = ((Object)_xifexpression);
    }
    return _xblockexpression;
  }
  
  public String addToHTML(final String s) {
    String _xblockexpression = null;
    {
      for (int i = 0; (i < this.whiteSpace); i++) {
        String _html = this.html;
        this.html = (_html + "\t");
      }
      String _html = this.html;
      _xblockexpression = this.html = (_html + s);
    }
    return _xblockexpression;
  }
  
  public void newLine() {
    boolean _endsWith = this.html.endsWith("\n");
    if (_endsWith) {
      return;
    }
    this.addToHTML("\n");
  }
}
