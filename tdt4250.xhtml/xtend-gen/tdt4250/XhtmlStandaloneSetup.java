/**
 * generated by Xtext 2.15.0
 */
package tdt4250;

import tdt4250.XhtmlStandaloneSetupGenerated;

/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
@SuppressWarnings("all")
public class XhtmlStandaloneSetup extends XhtmlStandaloneSetupGenerated {
  public static void doSetup() {
    new XhtmlStandaloneSetup().createInjectorAndDoEMFRegistration();
  }
}
