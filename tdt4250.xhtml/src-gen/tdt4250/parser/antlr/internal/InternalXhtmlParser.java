package tdt4250.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import tdt4250.services.XhtmlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXhtmlParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_COMMENT_TEXT", "RULE_ID", "RULE_STD_TEXT", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'/'", "'#'", "'.'", "'('", "')'", "'$'", "'='", "';'", "':'", "'1'", "'2'", "'3'", "'4'", "'5'", "'6'", "'7'", "'8'", "'9'", "'0'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_STD_TEXT=6;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_COMMENT_TEXT=4;
    public static final int RULE_ID=5;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalXhtmlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXhtmlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXhtmlParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXhtml.g"; }



     	private XhtmlGrammarAccess grammarAccess;

        public InternalXhtmlParser(TokenStream input, XhtmlGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";
       	}

       	@Override
       	protected XhtmlGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDomainmodel"
    // InternalXhtml.g:64:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // InternalXhtml.g:64:52: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // InternalXhtml.g:65:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalXhtml.g:71:1: ruleDomainmodel returns [EObject current=null] : ( (lv_elements_0_0= ruleType ) )* ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;



        	enterRule();

        try {
            // InternalXhtml.g:77:2: ( ( (lv_elements_0_0= ruleType ) )* )
            // InternalXhtml.g:78:2: ( (lv_elements_0_0= ruleType ) )*
            {
            // InternalXhtml.g:78:2: ( (lv_elements_0_0= ruleType ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_COMMENT_TEXT && LA1_0<=RULE_STD_TEXT)||LA1_0==13||LA1_0==18) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXhtml.g:79:3: (lv_elements_0_0= ruleType )
            	    {
            	    // InternalXhtml.g:79:3: (lv_elements_0_0= ruleType )
            	    // InternalXhtml.g:80:4: lv_elements_0_0= ruleType
            	    {

            	    				newCompositeNode(grammarAccess.getDomainmodelAccess().getElementsTypeParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_elements_0_0=ruleType();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"elements",
            	    					lv_elements_0_0,
            	    					"tdt4250.Xhtml.Type");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleType"
    // InternalXhtml.g:100:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalXhtml.g:100:45: (iv_ruleType= ruleType EOF )
            // InternalXhtml.g:101:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalXhtml.g:107:1: ruleType returns [EObject current=null] : (this_Comment_0= ruleComment | this_XmlElement_1= ruleXmlElement | this_InnerText_2= ruleInnerText | this_Variable_3= ruleVariable ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_Comment_0 = null;

        EObject this_XmlElement_1 = null;

        EObject this_InnerText_2 = null;

        EObject this_Variable_3 = null;



        	enterRule();

        try {
            // InternalXhtml.g:113:2: ( (this_Comment_0= ruleComment | this_XmlElement_1= ruleXmlElement | this_InnerText_2= ruleInnerText | this_Variable_3= ruleVariable ) )
            // InternalXhtml.g:114:2: (this_Comment_0= ruleComment | this_XmlElement_1= ruleXmlElement | this_InnerText_2= ruleInnerText | this_Variable_3= ruleVariable )
            {
            // InternalXhtml.g:114:2: (this_Comment_0= ruleComment | this_XmlElement_1= ruleXmlElement | this_InnerText_2= ruleInnerText | this_Variable_3= ruleVariable )
            int alt2=4;
            switch ( input.LA(1) ) {
            case RULE_COMMENT_TEXT:
                {
                alt2=1;
                }
                break;
            case 13:
                {
                alt2=2;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case RULE_COMMENT_TEXT:
                case 13:
                case 17:
                case 18:
                    {
                    alt2=3;
                    }
                    break;
                case RULE_ID:
                    {
                    int LA2_6 = input.LA(3);

                    if ( (LA2_6==EOF||(LA2_6>=RULE_COMMENT_TEXT && LA2_6<=RULE_STD_TEXT)||(LA2_6>=13 && LA2_6<=18)) ) {
                        alt2=3;
                    }
                    else if ( (LA2_6==19||LA2_6==21) ) {
                        alt2=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 6, input);

                        throw nvae;
                    }
                    }
                    break;
                case RULE_STD_TEXT:
                    {
                    int LA2_7 = input.LA(3);

                    if ( (LA2_7==21) ) {
                        alt2=2;
                    }
                    else if ( (LA2_7==EOF||(LA2_7>=RULE_COMMENT_TEXT && LA2_7<=RULE_STD_TEXT)||LA2_7==13||(LA2_7>=17 && LA2_7<=18)) ) {
                        alt2=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 7, input);

                        throw nvae;
                    }
                    }
                    break;
                case 14:
                case 15:
                case 16:
                    {
                    alt2=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 3, input);

                    throw nvae;
                }

                }
                break;
            case RULE_STD_TEXT:
                {
                alt2=3;
                }
                break;
            case 18:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalXhtml.g:115:3: this_Comment_0= ruleComment
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getCommentParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Comment_0=ruleComment();

                    state._fsp--;


                    			current = this_Comment_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:124:3: this_XmlElement_1= ruleXmlElement
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getXmlElementParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_XmlElement_1=ruleXmlElement();

                    state._fsp--;


                    			current = this_XmlElement_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalXhtml.g:133:3: this_InnerText_2= ruleInnerText
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getInnerTextParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_InnerText_2=ruleInnerText();

                    state._fsp--;


                    			current = this_InnerText_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalXhtml.g:142:3: this_Variable_3= ruleVariable
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getVariableParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_Variable_3=ruleVariable();

                    state._fsp--;


                    			current = this_Variable_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleComment"
    // InternalXhtml.g:154:1: entryRuleComment returns [EObject current=null] : iv_ruleComment= ruleComment EOF ;
    public final EObject entryRuleComment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComment = null;


        try {
            // InternalXhtml.g:154:48: (iv_ruleComment= ruleComment EOF )
            // InternalXhtml.g:155:2: iv_ruleComment= ruleComment EOF
            {
             newCompositeNode(grammarAccess.getCommentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComment=ruleComment();

            state._fsp--;

             current =iv_ruleComment; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComment"


    // $ANTLR start "ruleComment"
    // InternalXhtml.g:161:1: ruleComment returns [EObject current=null] : ( () ( (lv_text_1_0= RULE_COMMENT_TEXT ) ) ) ;
    public final EObject ruleComment() throws RecognitionException {
        EObject current = null;

        Token lv_text_1_0=null;


        	enterRule();

        try {
            // InternalXhtml.g:167:2: ( ( () ( (lv_text_1_0= RULE_COMMENT_TEXT ) ) ) )
            // InternalXhtml.g:168:2: ( () ( (lv_text_1_0= RULE_COMMENT_TEXT ) ) )
            {
            // InternalXhtml.g:168:2: ( () ( (lv_text_1_0= RULE_COMMENT_TEXT ) ) )
            // InternalXhtml.g:169:3: () ( (lv_text_1_0= RULE_COMMENT_TEXT ) )
            {
            // InternalXhtml.g:169:3: ()
            // InternalXhtml.g:170:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCommentAccess().getCommentAction_0(),
            					current);
            			

            }

            // InternalXhtml.g:176:3: ( (lv_text_1_0= RULE_COMMENT_TEXT ) )
            // InternalXhtml.g:177:4: (lv_text_1_0= RULE_COMMENT_TEXT )
            {
            // InternalXhtml.g:177:4: (lv_text_1_0= RULE_COMMENT_TEXT )
            // InternalXhtml.g:178:5: lv_text_1_0= RULE_COMMENT_TEXT
            {
            lv_text_1_0=(Token)match(input,RULE_COMMENT_TEXT,FOLLOW_2); 

            					newLeafNode(lv_text_1_0, grammarAccess.getCommentAccess().getTextCOMMENT_TEXTTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommentRule());
            					}
            					setWithLastConsumed(
            						current,
            						"text",
            						lv_text_1_0,
            						"tdt4250.Xhtml.COMMENT_TEXT");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComment"


    // $ANTLR start "entryRuleXmlElement"
    // InternalXhtml.g:198:1: entryRuleXmlElement returns [EObject current=null] : iv_ruleXmlElement= ruleXmlElement EOF ;
    public final EObject entryRuleXmlElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlElement = null;


        try {
            // InternalXhtml.g:198:51: (iv_ruleXmlElement= ruleXmlElement EOF )
            // InternalXhtml.g:199:2: iv_ruleXmlElement= ruleXmlElement EOF
            {
             newCompositeNode(grammarAccess.getXmlElementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlElement=ruleXmlElement();

            state._fsp--;

             current =iv_ruleXmlElement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlElement"


    // $ANTLR start "ruleXmlElement"
    // InternalXhtml.g:205:1: ruleXmlElement returns [EObject current=null] : ( ( (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) ) | ( (lv_type_2_0= RULE_ID ) ) ) (otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) ) )? (otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) ) )? ( (lv_attributes_7_0= ruleXmlAttribute ) )* ( (lv_styles_8_0= ruleXmlStyle ) )* otherlv_9= '(' ( (lv_contents_10_0= ruleType ) )* otherlv_11= ')' ) ;
    public final EObject ruleXmlElement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_sugarType_1_0=null;
        Token lv_type_2_0=null;
        Token otherlv_3=null;
        Token lv_id_4_0=null;
        Token otherlv_5=null;
        Token lv_className_6_0=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        EObject lv_attributes_7_0 = null;

        EObject lv_styles_8_0 = null;

        EObject lv_contents_10_0 = null;



        	enterRule();

        try {
            // InternalXhtml.g:211:2: ( ( ( (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) ) | ( (lv_type_2_0= RULE_ID ) ) ) (otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) ) )? (otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) ) )? ( (lv_attributes_7_0= ruleXmlAttribute ) )* ( (lv_styles_8_0= ruleXmlStyle ) )* otherlv_9= '(' ( (lv_contents_10_0= ruleType ) )* otherlv_11= ')' ) )
            // InternalXhtml.g:212:2: ( ( (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) ) | ( (lv_type_2_0= RULE_ID ) ) ) (otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) ) )? (otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) ) )? ( (lv_attributes_7_0= ruleXmlAttribute ) )* ( (lv_styles_8_0= ruleXmlStyle ) )* otherlv_9= '(' ( (lv_contents_10_0= ruleType ) )* otherlv_11= ')' )
            {
            // InternalXhtml.g:212:2: ( ( (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) ) | ( (lv_type_2_0= RULE_ID ) ) ) (otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) ) )? (otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) ) )? ( (lv_attributes_7_0= ruleXmlAttribute ) )* ( (lv_styles_8_0= ruleXmlStyle ) )* otherlv_9= '(' ( (lv_contents_10_0= ruleType ) )* otherlv_11= ')' )
            // InternalXhtml.g:213:3: ( (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) ) | ( (lv_type_2_0= RULE_ID ) ) ) (otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) ) )? (otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) ) )? ( (lv_attributes_7_0= ruleXmlAttribute ) )* ( (lv_styles_8_0= ruleXmlStyle ) )* otherlv_9= '(' ( (lv_contents_10_0= ruleType ) )* otherlv_11= ')'
            {
            // InternalXhtml.g:213:3: ( (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) ) | ( (lv_type_2_0= RULE_ID ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalXhtml.g:214:4: (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) )
                    {
                    // InternalXhtml.g:214:4: (otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) ) )
                    // InternalXhtml.g:215:5: otherlv_0= '/' ( (lv_sugarType_1_0= RULE_ID ) )
                    {
                    otherlv_0=(Token)match(input,13,FOLLOW_4); 

                    					newLeafNode(otherlv_0, grammarAccess.getXmlElementAccess().getSolidusKeyword_0_0_0());
                    				
                    // InternalXhtml.g:219:5: ( (lv_sugarType_1_0= RULE_ID ) )
                    // InternalXhtml.g:220:6: (lv_sugarType_1_0= RULE_ID )
                    {
                    // InternalXhtml.g:220:6: (lv_sugarType_1_0= RULE_ID )
                    // InternalXhtml.g:221:7: lv_sugarType_1_0= RULE_ID
                    {
                    lv_sugarType_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

                    							newLeafNode(lv_sugarType_1_0, grammarAccess.getXmlElementAccess().getSugarTypeIDTerminalRuleCall_0_0_1_0());
                    						

                    							if (current==null) {
                    								current = createModelElement(grammarAccess.getXmlElementRule());
                    							}
                    							setWithLastConsumed(
                    								current,
                    								"sugarType",
                    								lv_sugarType_1_0,
                    								"org.eclipse.xtext.common.Terminals.ID");
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:239:4: ( (lv_type_2_0= RULE_ID ) )
                    {
                    // InternalXhtml.g:239:4: ( (lv_type_2_0= RULE_ID ) )
                    // InternalXhtml.g:240:5: (lv_type_2_0= RULE_ID )
                    {
                    // InternalXhtml.g:240:5: (lv_type_2_0= RULE_ID )
                    // InternalXhtml.g:241:6: lv_type_2_0= RULE_ID
                    {
                    lv_type_2_0=(Token)match(input,RULE_ID,FOLLOW_5); 

                    						newLeafNode(lv_type_2_0, grammarAccess.getXmlElementAccess().getTypeIDTerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlElementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"type",
                    							lv_type_2_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXhtml.g:258:3: (otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalXhtml.g:259:4: otherlv_3= '#' ( (lv_id_4_0= RULE_ID ) )
                    {
                    otherlv_3=(Token)match(input,14,FOLLOW_4); 

                    				newLeafNode(otherlv_3, grammarAccess.getXmlElementAccess().getNumberSignKeyword_1_0());
                    			
                    // InternalXhtml.g:263:4: ( (lv_id_4_0= RULE_ID ) )
                    // InternalXhtml.g:264:5: (lv_id_4_0= RULE_ID )
                    {
                    // InternalXhtml.g:264:5: (lv_id_4_0= RULE_ID )
                    // InternalXhtml.g:265:6: lv_id_4_0= RULE_ID
                    {
                    lv_id_4_0=(Token)match(input,RULE_ID,FOLLOW_6); 

                    						newLeafNode(lv_id_4_0, grammarAccess.getXmlElementAccess().getIdIDTerminalRuleCall_1_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlElementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"id",
                    							lv_id_4_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXhtml.g:282:3: (otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) ) )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalXhtml.g:283:4: otherlv_5= '.' ( (lv_className_6_0= RULE_ID ) )
                    {
                    otherlv_5=(Token)match(input,15,FOLLOW_4); 

                    				newLeafNode(otherlv_5, grammarAccess.getXmlElementAccess().getFullStopKeyword_2_0());
                    			
                    // InternalXhtml.g:287:4: ( (lv_className_6_0= RULE_ID ) )
                    // InternalXhtml.g:288:5: (lv_className_6_0= RULE_ID )
                    {
                    // InternalXhtml.g:288:5: (lv_className_6_0= RULE_ID )
                    // InternalXhtml.g:289:6: lv_className_6_0= RULE_ID
                    {
                    lv_className_6_0=(Token)match(input,RULE_ID,FOLLOW_7); 

                    						newLeafNode(lv_className_6_0, grammarAccess.getXmlElementAccess().getClassNameIDTerminalRuleCall_2_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlElementRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"className",
                    							lv_className_6_0,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalXhtml.g:306:3: ( (lv_attributes_7_0= ruleXmlAttribute ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID) ) {
                    int LA6_1 = input.LA(2);

                    if ( (LA6_1==19) ) {
                        alt6=1;
                    }


                }


                switch (alt6) {
            	case 1 :
            	    // InternalXhtml.g:307:4: (lv_attributes_7_0= ruleXmlAttribute )
            	    {
            	    // InternalXhtml.g:307:4: (lv_attributes_7_0= ruleXmlAttribute )
            	    // InternalXhtml.g:308:5: lv_attributes_7_0= ruleXmlAttribute
            	    {

            	    					newCompositeNode(grammarAccess.getXmlElementAccess().getAttributesXmlAttributeParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_attributes_7_0=ruleXmlAttribute();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXmlElementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"attributes",
            	    						lv_attributes_7_0,
            	    						"tdt4250.Xhtml.XmlAttribute");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            // InternalXhtml.g:325:3: ( (lv_styles_8_0= ruleXmlStyle ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>=RULE_ID && LA7_0<=RULE_STD_TEXT)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalXhtml.g:326:4: (lv_styles_8_0= ruleXmlStyle )
            	    {
            	    // InternalXhtml.g:326:4: (lv_styles_8_0= ruleXmlStyle )
            	    // InternalXhtml.g:327:5: lv_styles_8_0= ruleXmlStyle
            	    {

            	    					newCompositeNode(grammarAccess.getXmlElementAccess().getStylesXmlStyleParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_7);
            	    lv_styles_8_0=ruleXmlStyle();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXmlElementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"styles",
            	    						lv_styles_8_0,
            	    						"tdt4250.Xhtml.XmlStyle");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_9=(Token)match(input,16,FOLLOW_8); 

            			newLeafNode(otherlv_9, grammarAccess.getXmlElementAccess().getLeftParenthesisKeyword_5());
            		
            // InternalXhtml.g:348:3: ( (lv_contents_10_0= ruleType ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_COMMENT_TEXT && LA8_0<=RULE_STD_TEXT)||LA8_0==13||LA8_0==18) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalXhtml.g:349:4: (lv_contents_10_0= ruleType )
            	    {
            	    // InternalXhtml.g:349:4: (lv_contents_10_0= ruleType )
            	    // InternalXhtml.g:350:5: lv_contents_10_0= ruleType
            	    {

            	    					newCompositeNode(grammarAccess.getXmlElementAccess().getContentsTypeParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_contents_10_0=ruleType();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXmlElementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"contents",
            	    						lv_contents_10_0,
            	    						"tdt4250.Xhtml.Type");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_11=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getXmlElementAccess().getRightParenthesisKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlElement"


    // $ANTLR start "entryRuleInnerText"
    // InternalXhtml.g:375:1: entryRuleInnerText returns [EObject current=null] : iv_ruleInnerText= ruleInnerText EOF ;
    public final EObject entryRuleInnerText() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInnerText = null;


        try {
            // InternalXhtml.g:375:50: (iv_ruleInnerText= ruleInnerText EOF )
            // InternalXhtml.g:376:2: iv_ruleInnerText= ruleInnerText EOF
            {
             newCompositeNode(grammarAccess.getInnerTextRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInnerText=ruleInnerText();

            state._fsp--;

             current =iv_ruleInnerText; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInnerText"


    // $ANTLR start "ruleInnerText"
    // InternalXhtml.g:382:1: ruleInnerText returns [EObject current=null] : ( () ( ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) ) ) ) ;
    public final EObject ruleInnerText() throws RecognitionException {
        EObject current = null;

        Token lv_text_1_1=null;
        Token lv_text_1_2=null;


        	enterRule();

        try {
            // InternalXhtml.g:388:2: ( ( () ( ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) ) ) ) )
            // InternalXhtml.g:389:2: ( () ( ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) ) ) )
            {
            // InternalXhtml.g:389:2: ( () ( ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) ) ) )
            // InternalXhtml.g:390:3: () ( ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) ) )
            {
            // InternalXhtml.g:390:3: ()
            // InternalXhtml.g:391:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInnerTextAccess().getInnerTextAction_0(),
            					current);
            			

            }

            // InternalXhtml.g:397:3: ( ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) ) )
            // InternalXhtml.g:398:4: ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) )
            {
            // InternalXhtml.g:398:4: ( (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID ) )
            // InternalXhtml.g:399:5: (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID )
            {
            // InternalXhtml.g:399:5: (lv_text_1_1= RULE_STD_TEXT | lv_text_1_2= RULE_ID )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_STD_TEXT) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_ID) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalXhtml.g:400:6: lv_text_1_1= RULE_STD_TEXT
                    {
                    lv_text_1_1=(Token)match(input,RULE_STD_TEXT,FOLLOW_2); 

                    						newLeafNode(lv_text_1_1, grammarAccess.getInnerTextAccess().getTextSTD_TEXTTerminalRuleCall_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getInnerTextRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"text",
                    							lv_text_1_1,
                    							"tdt4250.Xhtml.STD_TEXT");
                    					

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:415:6: lv_text_1_2= RULE_ID
                    {
                    lv_text_1_2=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_text_1_2, grammarAccess.getInnerTextAccess().getTextIDTerminalRuleCall_1_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getInnerTextRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"text",
                    							lv_text_1_2,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInnerText"


    // $ANTLR start "entryRuleVariable"
    // InternalXhtml.g:436:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalXhtml.g:436:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalXhtml.g:437:2: iv_ruleVariable= ruleVariable EOF
            {
             newCompositeNode(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;

             current =iv_ruleVariable; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalXhtml.g:443:1: ruleVariable returns [EObject current=null] : (otherlv_0= '$' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) ) )? otherlv_4= ';' ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_value_3_1=null;
        Token lv_value_3_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_value_3_3 = null;



        	enterRule();

        try {
            // InternalXhtml.g:449:2: ( (otherlv_0= '$' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) ) )? otherlv_4= ';' ) )
            // InternalXhtml.g:450:2: (otherlv_0= '$' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) ) )? otherlv_4= ';' )
            {
            // InternalXhtml.g:450:2: (otherlv_0= '$' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) ) )? otherlv_4= ';' )
            // InternalXhtml.g:451:3: otherlv_0= '$' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) ) )? otherlv_4= ';'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_4); 

            			newLeafNode(otherlv_0, grammarAccess.getVariableAccess().getDollarSignKeyword_0());
            		
            // InternalXhtml.g:455:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalXhtml.g:456:4: (lv_name_1_0= RULE_ID )
            {
            // InternalXhtml.g:456:4: (lv_name_1_0= RULE_ID )
            // InternalXhtml.g:457:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getVariableRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalXhtml.g:473:3: (otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==19) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalXhtml.g:474:4: otherlv_2= '=' ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) )
                    {
                    otherlv_2=(Token)match(input,19,FOLLOW_10); 

                    				newLeafNode(otherlv_2, grammarAccess.getVariableAccess().getEqualsSignKeyword_2_0());
                    			
                    // InternalXhtml.g:478:4: ( ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) ) )
                    // InternalXhtml.g:479:5: ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) )
                    {
                    // InternalXhtml.g:479:5: ( (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt ) )
                    // InternalXhtml.g:480:6: (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt )
                    {
                    // InternalXhtml.g:480:6: (lv_value_3_1= RULE_ID | lv_value_3_2= RULE_STD_TEXT | lv_value_3_3= ruleXmlInt )
                    int alt10=3;
                    switch ( input.LA(1) ) {
                    case RULE_ID:
                        {
                        alt10=1;
                        }
                        break;
                    case RULE_STD_TEXT:
                        {
                        alt10=2;
                        }
                        break;
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                        {
                        alt10=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 10, 0, input);

                        throw nvae;
                    }

                    switch (alt10) {
                        case 1 :
                            // InternalXhtml.g:481:7: lv_value_3_1= RULE_ID
                            {
                            lv_value_3_1=(Token)match(input,RULE_ID,FOLLOW_11); 

                            							newLeafNode(lv_value_3_1, grammarAccess.getVariableAccess().getValueIDTerminalRuleCall_2_1_0_0());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getVariableRule());
                            							}
                            							setWithLastConsumed(
                            								current,
                            								"value",
                            								lv_value_3_1,
                            								"org.eclipse.xtext.common.Terminals.ID");
                            						

                            }
                            break;
                        case 2 :
                            // InternalXhtml.g:496:7: lv_value_3_2= RULE_STD_TEXT
                            {
                            lv_value_3_2=(Token)match(input,RULE_STD_TEXT,FOLLOW_11); 

                            							newLeafNode(lv_value_3_2, grammarAccess.getVariableAccess().getValueSTD_TEXTTerminalRuleCall_2_1_0_1());
                            						

                            							if (current==null) {
                            								current = createModelElement(grammarAccess.getVariableRule());
                            							}
                            							setWithLastConsumed(
                            								current,
                            								"value",
                            								lv_value_3_2,
                            								"tdt4250.Xhtml.STD_TEXT");
                            						

                            }
                            break;
                        case 3 :
                            // InternalXhtml.g:511:7: lv_value_3_3= ruleXmlInt
                            {

                            							newCompositeNode(grammarAccess.getVariableAccess().getValueXmlIntParserRuleCall_2_1_0_2());
                            						
                            pushFollow(FOLLOW_11);
                            lv_value_3_3=ruleXmlInt();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getVariableRule());
                            							}
                            							set(
                            								current,
                            								"value",
                            								lv_value_3_3,
                            								"tdt4250.Xhtml.XmlInt");
                            							afterParserOrEnumRuleCall();
                            						

                            }
                            break;

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,20,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getVariableAccess().getSemicolonKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleXmlAttribute"
    // InternalXhtml.g:538:1: entryRuleXmlAttribute returns [EObject current=null] : iv_ruleXmlAttribute= ruleXmlAttribute EOF ;
    public final EObject entryRuleXmlAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlAttribute = null;


        try {
            // InternalXhtml.g:538:53: (iv_ruleXmlAttribute= ruleXmlAttribute EOF )
            // InternalXhtml.g:539:2: iv_ruleXmlAttribute= ruleXmlAttribute EOF
            {
             newCompositeNode(grammarAccess.getXmlAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlAttribute=ruleXmlAttribute();

            state._fsp--;

             current =iv_ruleXmlAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlAttribute"


    // $ANTLR start "ruleXmlAttribute"
    // InternalXhtml.g:545:1: ruleXmlAttribute returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) ) ) ) ;
    public final EObject ruleXmlAttribute() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_1=null;
        Token lv_value_2_2=null;
        AntlrDatatypeRuleToken lv_value_2_3 = null;



        	enterRule();

        try {
            // InternalXhtml.g:551:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) ) ) ) )
            // InternalXhtml.g:552:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) ) ) )
            {
            // InternalXhtml.g:552:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) ) ) )
            // InternalXhtml.g:553:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= '=' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) ) )
            {
            // InternalXhtml.g:553:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalXhtml.g:554:4: (lv_name_0_0= RULE_ID )
            {
            // InternalXhtml.g:554:4: (lv_name_0_0= RULE_ID )
            // InternalXhtml.g:555:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_12); 

            					newLeafNode(lv_name_0_0, grammarAccess.getXmlAttributeAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getXmlAttributeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,19,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getXmlAttributeAccess().getEqualsSignKeyword_1());
            		
            // InternalXhtml.g:575:3: ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) ) )
            // InternalXhtml.g:576:4: ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) )
            {
            // InternalXhtml.g:576:4: ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt ) )
            // InternalXhtml.g:577:5: (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt )
            {
            // InternalXhtml.g:577:5: (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT | lv_value_2_3= ruleXmlInt )
            int alt12=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt12=1;
                }
                break;
            case RULE_STD_TEXT:
                {
                alt12=2;
                }
                break;
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalXhtml.g:578:6: lv_value_2_1= RULE_ID
                    {
                    lv_value_2_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_value_2_1, grammarAccess.getXmlAttributeAccess().getValueIDTerminalRuleCall_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_2_1,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:593:6: lv_value_2_2= RULE_STD_TEXT
                    {
                    lv_value_2_2=(Token)match(input,RULE_STD_TEXT,FOLLOW_2); 

                    						newLeafNode(lv_value_2_2, grammarAccess.getXmlAttributeAccess().getValueSTD_TEXTTerminalRuleCall_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlAttributeRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_2_2,
                    							"tdt4250.Xhtml.STD_TEXT");
                    					

                    }
                    break;
                case 3 :
                    // InternalXhtml.g:608:6: lv_value_2_3= ruleXmlInt
                    {

                    						newCompositeNode(grammarAccess.getXmlAttributeAccess().getValueXmlIntParserRuleCall_2_0_2());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_2_3=ruleXmlInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getXmlAttributeRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_2_3,
                    							"tdt4250.Xhtml.XmlInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlAttribute"


    // $ANTLR start "entryRuleXmlStyle"
    // InternalXhtml.g:630:1: entryRuleXmlStyle returns [EObject current=null] : iv_ruleXmlStyle= ruleXmlStyle EOF ;
    public final EObject entryRuleXmlStyle() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXmlStyle = null;


        try {
            // InternalXhtml.g:630:49: (iv_ruleXmlStyle= ruleXmlStyle EOF )
            // InternalXhtml.g:631:2: iv_ruleXmlStyle= ruleXmlStyle EOF
            {
             newCompositeNode(grammarAccess.getXmlStyleRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlStyle=ruleXmlStyle();

            state._fsp--;

             current =iv_ruleXmlStyle; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlStyle"


    // $ANTLR start "ruleXmlStyle"
    // InternalXhtml.g:637:1: ruleXmlStyle returns [EObject current=null] : ( ( ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) ) ) otherlv_1= ':' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) ) ) ) ;
    public final EObject ruleXmlStyle() throws RecognitionException {
        EObject current = null;

        Token lv_attribute_0_1=null;
        Token lv_attribute_0_2=null;
        Token otherlv_1=null;
        Token lv_value_2_1=null;
        Token lv_value_2_2=null;


        	enterRule();

        try {
            // InternalXhtml.g:643:2: ( ( ( ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) ) ) otherlv_1= ':' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) ) ) ) )
            // InternalXhtml.g:644:2: ( ( ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) ) ) otherlv_1= ':' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) ) ) )
            {
            // InternalXhtml.g:644:2: ( ( ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) ) ) otherlv_1= ':' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) ) ) )
            // InternalXhtml.g:645:3: ( ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) ) ) otherlv_1= ':' ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) ) )
            {
            // InternalXhtml.g:645:3: ( ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) ) )
            // InternalXhtml.g:646:4: ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) )
            {
            // InternalXhtml.g:646:4: ( (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT ) )
            // InternalXhtml.g:647:5: (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT )
            {
            // InternalXhtml.g:647:5: (lv_attribute_0_1= RULE_ID | lv_attribute_0_2= RULE_STD_TEXT )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==RULE_ID) ) {
                alt13=1;
            }
            else if ( (LA13_0==RULE_STD_TEXT) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalXhtml.g:648:6: lv_attribute_0_1= RULE_ID
                    {
                    lv_attribute_0_1=(Token)match(input,RULE_ID,FOLLOW_13); 

                    						newLeafNode(lv_attribute_0_1, grammarAccess.getXmlStyleAccess().getAttributeIDTerminalRuleCall_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlStyleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"attribute",
                    							lv_attribute_0_1,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:663:6: lv_attribute_0_2= RULE_STD_TEXT
                    {
                    lv_attribute_0_2=(Token)match(input,RULE_STD_TEXT,FOLLOW_13); 

                    						newLeafNode(lv_attribute_0_2, grammarAccess.getXmlStyleAccess().getAttributeSTD_TEXTTerminalRuleCall_0_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlStyleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"attribute",
                    							lv_attribute_0_2,
                    							"tdt4250.Xhtml.STD_TEXT");
                    					

                    }
                    break;

            }


            }


            }

            otherlv_1=(Token)match(input,21,FOLLOW_14); 

            			newLeafNode(otherlv_1, grammarAccess.getXmlStyleAccess().getColonKeyword_1());
            		
            // InternalXhtml.g:684:3: ( ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) ) )
            // InternalXhtml.g:685:4: ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) )
            {
            // InternalXhtml.g:685:4: ( (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT ) )
            // InternalXhtml.g:686:5: (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT )
            {
            // InternalXhtml.g:686:5: (lv_value_2_1= RULE_ID | lv_value_2_2= RULE_STD_TEXT )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==RULE_ID) ) {
                alt14=1;
            }
            else if ( (LA14_0==RULE_STD_TEXT) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalXhtml.g:687:6: lv_value_2_1= RULE_ID
                    {
                    lv_value_2_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    						newLeafNode(lv_value_2_1, grammarAccess.getXmlStyleAccess().getValueIDTerminalRuleCall_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlStyleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_2_1,
                    							"org.eclipse.xtext.common.Terminals.ID");
                    					

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:702:6: lv_value_2_2= RULE_STD_TEXT
                    {
                    lv_value_2_2=(Token)match(input,RULE_STD_TEXT,FOLLOW_2); 

                    						newLeafNode(lv_value_2_2, grammarAccess.getXmlStyleAccess().getValueSTD_TEXTTerminalRuleCall_2_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getXmlStyleRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"value",
                    							lv_value_2_2,
                    							"tdt4250.Xhtml.STD_TEXT");
                    					

                    }
                    break;

            }


            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlStyle"


    // $ANTLR start "entryRuleXmlInt"
    // InternalXhtml.g:723:1: entryRuleXmlInt returns [String current=null] : iv_ruleXmlInt= ruleXmlInt EOF ;
    public final String entryRuleXmlInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleXmlInt = null;


        try {
            // InternalXhtml.g:723:46: (iv_ruleXmlInt= ruleXmlInt EOF )
            // InternalXhtml.g:724:2: iv_ruleXmlInt= ruleXmlInt EOF
            {
             newCompositeNode(grammarAccess.getXmlIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXmlInt=ruleXmlInt();

            state._fsp--;

             current =iv_ruleXmlInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXmlInt"


    // $ANTLR start "ruleXmlInt"
    // InternalXhtml.g:730:1: ruleXmlInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' ) (kw= '0' | kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )* ) ;
    public final AntlrDatatypeRuleToken ruleXmlInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXhtml.g:736:2: ( ( (kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' ) (kw= '0' | kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )* ) )
            // InternalXhtml.g:737:2: ( (kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' ) (kw= '0' | kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )* )
            {
            // InternalXhtml.g:737:2: ( (kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' ) (kw= '0' | kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )* )
            // InternalXhtml.g:738:3: (kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' ) (kw= '0' | kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )*
            {
            // InternalXhtml.g:738:3: (kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )
            int alt15=9;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt15=1;
                }
                break;
            case 23:
                {
                alt15=2;
                }
                break;
            case 24:
                {
                alt15=3;
                }
                break;
            case 25:
                {
                alt15=4;
                }
                break;
            case 26:
                {
                alt15=5;
                }
                break;
            case 27:
                {
                alt15=6;
                }
                break;
            case 28:
                {
                alt15=7;
                }
                break;
            case 29:
                {
                alt15=8;
                }
                break;
            case 30:
                {
                alt15=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalXhtml.g:739:4: kw= '1'
                    {
                    kw=(Token)match(input,22,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitOneKeyword_0_0());
                    			

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:745:4: kw= '2'
                    {
                    kw=(Token)match(input,23,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitTwoKeyword_0_1());
                    			

                    }
                    break;
                case 3 :
                    // InternalXhtml.g:751:4: kw= '3'
                    {
                    kw=(Token)match(input,24,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitThreeKeyword_0_2());
                    			

                    }
                    break;
                case 4 :
                    // InternalXhtml.g:757:4: kw= '4'
                    {
                    kw=(Token)match(input,25,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitFourKeyword_0_3());
                    			

                    }
                    break;
                case 5 :
                    // InternalXhtml.g:763:4: kw= '5'
                    {
                    kw=(Token)match(input,26,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitFiveKeyword_0_4());
                    			

                    }
                    break;
                case 6 :
                    // InternalXhtml.g:769:4: kw= '6'
                    {
                    kw=(Token)match(input,27,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitSixKeyword_0_5());
                    			

                    }
                    break;
                case 7 :
                    // InternalXhtml.g:775:4: kw= '7'
                    {
                    kw=(Token)match(input,28,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitSevenKeyword_0_6());
                    			

                    }
                    break;
                case 8 :
                    // InternalXhtml.g:781:4: kw= '8'
                    {
                    kw=(Token)match(input,29,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitEightKeyword_0_7());
                    			

                    }
                    break;
                case 9 :
                    // InternalXhtml.g:787:4: kw= '9'
                    {
                    kw=(Token)match(input,30,FOLLOW_15); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitNineKeyword_0_8());
                    			

                    }
                    break;

            }

            // InternalXhtml.g:793:3: (kw= '0' | kw= '1' | kw= '2' | kw= '3' | kw= '4' | kw= '5' | kw= '6' | kw= '7' | kw= '8' | kw= '9' )*
            loop16:
            do {
                int alt16=11;
                switch ( input.LA(1) ) {
                case 31:
                    {
                    alt16=1;
                    }
                    break;
                case 22:
                    {
                    alt16=2;
                    }
                    break;
                case 23:
                    {
                    alt16=3;
                    }
                    break;
                case 24:
                    {
                    alt16=4;
                    }
                    break;
                case 25:
                    {
                    alt16=5;
                    }
                    break;
                case 26:
                    {
                    alt16=6;
                    }
                    break;
                case 27:
                    {
                    alt16=7;
                    }
                    break;
                case 28:
                    {
                    alt16=8;
                    }
                    break;
                case 29:
                    {
                    alt16=9;
                    }
                    break;
                case 30:
                    {
                    alt16=10;
                    }
                    break;

                }

                switch (alt16) {
            	case 1 :
            	    // InternalXhtml.g:794:4: kw= '0'
            	    {
            	    kw=(Token)match(input,31,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitZeroKeyword_1_0());
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalXhtml.g:800:4: kw= '1'
            	    {
            	    kw=(Token)match(input,22,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitOneKeyword_1_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalXhtml.g:806:4: kw= '2'
            	    {
            	    kw=(Token)match(input,23,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitTwoKeyword_1_2());
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalXhtml.g:812:4: kw= '3'
            	    {
            	    kw=(Token)match(input,24,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitThreeKeyword_1_3());
            	    			

            	    }
            	    break;
            	case 5 :
            	    // InternalXhtml.g:818:4: kw= '4'
            	    {
            	    kw=(Token)match(input,25,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitFourKeyword_1_4());
            	    			

            	    }
            	    break;
            	case 6 :
            	    // InternalXhtml.g:824:4: kw= '5'
            	    {
            	    kw=(Token)match(input,26,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitFiveKeyword_1_5());
            	    			

            	    }
            	    break;
            	case 7 :
            	    // InternalXhtml.g:830:4: kw= '6'
            	    {
            	    kw=(Token)match(input,27,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitSixKeyword_1_6());
            	    			

            	    }
            	    break;
            	case 8 :
            	    // InternalXhtml.g:836:4: kw= '7'
            	    {
            	    kw=(Token)match(input,28,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitSevenKeyword_1_7());
            	    			

            	    }
            	    break;
            	case 9 :
            	    // InternalXhtml.g:842:4: kw= '8'
            	    {
            	    kw=(Token)match(input,29,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitEightKeyword_1_8());
            	    			

            	    }
            	    break;
            	case 10 :
            	    // InternalXhtml.g:848:4: kw= '9'
            	    {
            	    kw=(Token)match(input,30,FOLLOW_15); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getXmlIntAccess().getDigitNineKeyword_1_9());
            	    			

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXmlInt"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000042072L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000001C060L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000018060L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010060L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000062070L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000007FC00060L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000060L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x00000000FFC00002L});

}