/**
 * generated by Xtext 2.15.0
 */
package tdt4250.xhtml.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import tdt4250.xhtml.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XhtmlFactoryImpl extends EFactoryImpl implements XhtmlFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static XhtmlFactory init()
  {
    try
    {
      XhtmlFactory theXhtmlFactory = (XhtmlFactory)EPackage.Registry.INSTANCE.getEFactory(XhtmlPackage.eNS_URI);
      if (theXhtmlFactory != null)
      {
        return theXhtmlFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new XhtmlFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XhtmlFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case XhtmlPackage.DOMAINMODEL: return createDomainmodel();
      case XhtmlPackage.TYPE: return createType();
      case XhtmlPackage.COMMENT: return createComment();
      case XhtmlPackage.XML_ELEMENT: return createXmlElement();
      case XhtmlPackage.INNER_TEXT: return createInnerText();
      case XhtmlPackage.VARIABLE: return createVariable();
      case XhtmlPackage.XML_ATTRIBUTE: return createXmlAttribute();
      case XhtmlPackage.XML_STYLE: return createXmlStyle();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Domainmodel createDomainmodel()
  {
    DomainmodelImpl domainmodel = new DomainmodelImpl();
    return domainmodel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Comment createComment()
  {
    CommentImpl comment = new CommentImpl();
    return comment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XmlElement createXmlElement()
  {
    XmlElementImpl xmlElement = new XmlElementImpl();
    return xmlElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InnerText createInnerText()
  {
    InnerTextImpl innerText = new InnerTextImpl();
    return innerText;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XmlAttribute createXmlAttribute()
  {
    XmlAttributeImpl xmlAttribute = new XmlAttributeImpl();
    return xmlAttribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XmlStyle createXmlStyle()
  {
    XmlStyleImpl xmlStyle = new XmlStyleImpl();
    return xmlStyle;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XhtmlPackage getXhtmlPackage()
  {
    return (XhtmlPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static XhtmlPackage getPackage()
  {
    return XhtmlPackage.eINSTANCE;
  }

} //XhtmlFactoryImpl
