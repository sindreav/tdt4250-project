package tdt4250.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXhtmlLexer extends Lexer {
    public static final int RULE_STRING=8;
    public static final int RULE_STD_TEXT=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_COMMENT_TEXT=6;
    public static final int RULE_ID=5;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalXhtmlLexer() {;} 
    public InternalXhtmlLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalXhtmlLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalXhtml.g"; }

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:11:7: ( '1' )
            // InternalXhtml.g:11:9: '1'
            {
            match('1'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:12:7: ( '2' )
            // InternalXhtml.g:12:9: '2'
            {
            match('2'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:13:7: ( '3' )
            // InternalXhtml.g:13:9: '3'
            {
            match('3'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:14:7: ( '4' )
            // InternalXhtml.g:14:9: '4'
            {
            match('4'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:15:7: ( '5' )
            // InternalXhtml.g:15:9: '5'
            {
            match('5'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:16:7: ( '6' )
            // InternalXhtml.g:16:9: '6'
            {
            match('6'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:17:7: ( '7' )
            // InternalXhtml.g:17:9: '7'
            {
            match('7'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:18:7: ( '8' )
            // InternalXhtml.g:18:9: '8'
            {
            match('8'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:19:7: ( '9' )
            // InternalXhtml.g:19:9: '9'
            {
            match('9'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:20:7: ( '0' )
            // InternalXhtml.g:20:9: '0'
            {
            match('0'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:21:7: ( '(' )
            // InternalXhtml.g:21:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:22:7: ( ')' )
            // InternalXhtml.g:22:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:23:7: ( '/' )
            // InternalXhtml.g:23:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:24:7: ( '#' )
            // InternalXhtml.g:24:9: '#'
            {
            match('#'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:25:7: ( '.' )
            // InternalXhtml.g:25:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:26:7: ( '$' )
            // InternalXhtml.g:26:9: '$'
            {
            match('$'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:27:7: ( ';' )
            // InternalXhtml.g:27:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:28:7: ( '=' )
            // InternalXhtml.g:28:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:29:7: ( ':' )
            // InternalXhtml.g:29:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "RULE_COMMENT_TEXT"
    public final void mRULE_COMMENT_TEXT() throws RecognitionException {
        try {
            int _type = RULE_COMMENT_TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1684:19: ( '%' ~ ( '%' ) ( options {greedy=false; } : . )* '%' )
            // InternalXhtml.g:1684:21: '%' ~ ( '%' ) ( options {greedy=false; } : . )* '%'
            {
            match('%'); 
            if ( (input.LA(1)>='\u0000' && input.LA(1)<='$')||(input.LA(1)>='&' && input.LA(1)<='\uFFFF') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalXhtml.g:1684:32: ( options {greedy=false; } : . )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='%') ) {
                    alt1=2;
                }
                else if ( ((LA1_0>='\u0000' && LA1_0<='$')||(LA1_0>='&' && LA1_0<='\uFFFF')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXhtml.g:1684:60: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_COMMENT_TEXT"

    // $ANTLR start "RULE_STD_TEXT"
    public final void mRULE_STD_TEXT() throws RecognitionException {
        try {
            int _type = RULE_STD_TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1686:15: ( ( '\"' ~ ( '\"' ) ( options {greedy=false; } : . )* '\"' | '\\'' ~ ( '\\'' ) ( options {greedy=false; } : . )* '\\'' ) )
            // InternalXhtml.g:1686:17: ( '\"' ~ ( '\"' ) ( options {greedy=false; } : . )* '\"' | '\\'' ~ ( '\\'' ) ( options {greedy=false; } : . )* '\\'' )
            {
            // InternalXhtml.g:1686:17: ( '\"' ~ ( '\"' ) ( options {greedy=false; } : . )* '\"' | '\\'' ~ ( '\\'' ) ( options {greedy=false; } : . )* '\\'' )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\"') ) {
                alt4=1;
            }
            else if ( (LA4_0=='\'') ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalXhtml.g:1686:18: '\"' ~ ( '\"' ) ( options {greedy=false; } : . )* '\"'
                    {
                    match('\"'); 
                    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='\uFFFF') ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // InternalXhtml.g:1686:29: ( options {greedy=false; } : . )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0=='\"') ) {
                            alt2=2;
                        }
                        else if ( ((LA2_0>='\u0000' && LA2_0<='!')||(LA2_0>='#' && LA2_0<='\uFFFF')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalXhtml.g:1686:57: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:1686:65: '\\'' ~ ( '\\'' ) ( options {greedy=false; } : . )* '\\''
                    {
                    match('\''); 
                    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='\uFFFF') ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // InternalXhtml.g:1686:78: ( options {greedy=false; } : . )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0=='\'') ) {
                            alt3=2;
                        }
                        else if ( ((LA3_0>='\u0000' && LA3_0<='&')||(LA3_0>='(' && LA3_0<='\uFFFF')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalXhtml.g:1686:106: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STD_TEXT"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1688:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalXhtml.g:1688:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalXhtml.g:1688:11: ( '^' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='^') ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalXhtml.g:1688:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalXhtml.g:1688:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')||(LA6_0>='A' && LA6_0<='Z')||LA6_0=='_'||(LA6_0>='a' && LA6_0<='z')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalXhtml.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1690:10: ( ( '0' .. '9' )+ )
            // InternalXhtml.g:1690:12: ( '0' .. '9' )+
            {
            // InternalXhtml.g:1690:12: ( '0' .. '9' )+
            int cnt7=0;
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( ((LA7_0>='0' && LA7_0<='9')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalXhtml.g:1690:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt7 >= 1 ) break loop7;
                        EarlyExitException eee =
                            new EarlyExitException(7, input);
                        throw eee;
                }
                cnt7++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1692:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalXhtml.g:1692:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalXhtml.g:1692:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\"') ) {
                alt10=1;
            }
            else if ( (LA10_0=='\'') ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalXhtml.g:1692:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalXhtml.g:1692:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop8:
                    do {
                        int alt8=3;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='\\') ) {
                            alt8=1;
                        }
                        else if ( ((LA8_0>='\u0000' && LA8_0<='!')||(LA8_0>='#' && LA8_0<='[')||(LA8_0>=']' && LA8_0<='\uFFFF')) ) {
                            alt8=2;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // InternalXhtml.g:1692:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalXhtml.g:1692:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalXhtml.g:1692:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalXhtml.g:1692:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop9:
                    do {
                        int alt9=3;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0=='\\') ) {
                            alt9=1;
                        }
                        else if ( ((LA9_0>='\u0000' && LA9_0<='&')||(LA9_0>='(' && LA9_0<='[')||(LA9_0>=']' && LA9_0<='\uFFFF')) ) {
                            alt9=2;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // InternalXhtml.g:1692:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalXhtml.g:1692:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1694:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalXhtml.g:1694:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalXhtml.g:1694:24: ( options {greedy=false; } : . )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0=='*') ) {
                    int LA11_1 = input.LA(2);

                    if ( (LA11_1=='/') ) {
                        alt11=2;
                    }
                    else if ( ((LA11_1>='\u0000' && LA11_1<='.')||(LA11_1>='0' && LA11_1<='\uFFFF')) ) {
                        alt11=1;
                    }


                }
                else if ( ((LA11_0>='\u0000' && LA11_0<=')')||(LA11_0>='+' && LA11_0<='\uFFFF')) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalXhtml.g:1694:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1696:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalXhtml.g:1696:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalXhtml.g:1696:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\u0000' && LA12_0<='\t')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\uFFFF')) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalXhtml.g:1696:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

            // InternalXhtml.g:1696:40: ( ( '\\r' )? '\\n' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0=='\n'||LA14_0=='\r') ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalXhtml.g:1696:41: ( '\\r' )? '\\n'
                    {
                    // InternalXhtml.g:1696:41: ( '\\r' )?
                    int alt13=2;
                    int LA13_0 = input.LA(1);

                    if ( (LA13_0=='\r') ) {
                        alt13=1;
                    }
                    switch (alt13) {
                        case 1 :
                            // InternalXhtml.g:1696:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1698:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalXhtml.g:1698:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalXhtml.g:1698:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt15=0;
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>='\t' && LA15_0<='\n')||LA15_0=='\r'||LA15_0==' ') ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalXhtml.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt15 >= 1 ) break loop15;
                        EarlyExitException eee =
                            new EarlyExitException(15, input);
                        throw eee;
                }
                cnt15++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXhtml.g:1700:16: ( . )
            // InternalXhtml.g:1700:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalXhtml.g:1:8: ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | RULE_COMMENT_TEXT | RULE_STD_TEXT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt16=28;
        alt16 = dfa16.predict(input);
        switch (alt16) {
            case 1 :
                // InternalXhtml.g:1:10: T__13
                {
                mT__13(); 

                }
                break;
            case 2 :
                // InternalXhtml.g:1:16: T__14
                {
                mT__14(); 

                }
                break;
            case 3 :
                // InternalXhtml.g:1:22: T__15
                {
                mT__15(); 

                }
                break;
            case 4 :
                // InternalXhtml.g:1:28: T__16
                {
                mT__16(); 

                }
                break;
            case 5 :
                // InternalXhtml.g:1:34: T__17
                {
                mT__17(); 

                }
                break;
            case 6 :
                // InternalXhtml.g:1:40: T__18
                {
                mT__18(); 

                }
                break;
            case 7 :
                // InternalXhtml.g:1:46: T__19
                {
                mT__19(); 

                }
                break;
            case 8 :
                // InternalXhtml.g:1:52: T__20
                {
                mT__20(); 

                }
                break;
            case 9 :
                // InternalXhtml.g:1:58: T__21
                {
                mT__21(); 

                }
                break;
            case 10 :
                // InternalXhtml.g:1:64: T__22
                {
                mT__22(); 

                }
                break;
            case 11 :
                // InternalXhtml.g:1:70: T__23
                {
                mT__23(); 

                }
                break;
            case 12 :
                // InternalXhtml.g:1:76: T__24
                {
                mT__24(); 

                }
                break;
            case 13 :
                // InternalXhtml.g:1:82: T__25
                {
                mT__25(); 

                }
                break;
            case 14 :
                // InternalXhtml.g:1:88: T__26
                {
                mT__26(); 

                }
                break;
            case 15 :
                // InternalXhtml.g:1:94: T__27
                {
                mT__27(); 

                }
                break;
            case 16 :
                // InternalXhtml.g:1:100: T__28
                {
                mT__28(); 

                }
                break;
            case 17 :
                // InternalXhtml.g:1:106: T__29
                {
                mT__29(); 

                }
                break;
            case 18 :
                // InternalXhtml.g:1:112: T__30
                {
                mT__30(); 

                }
                break;
            case 19 :
                // InternalXhtml.g:1:118: T__31
                {
                mT__31(); 

                }
                break;
            case 20 :
                // InternalXhtml.g:1:124: RULE_COMMENT_TEXT
                {
                mRULE_COMMENT_TEXT(); 

                }
                break;
            case 21 :
                // InternalXhtml.g:1:142: RULE_STD_TEXT
                {
                mRULE_STD_TEXT(); 

                }
                break;
            case 22 :
                // InternalXhtml.g:1:156: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 23 :
                // InternalXhtml.g:1:164: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 24 :
                // InternalXhtml.g:1:173: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 25 :
                // InternalXhtml.g:1:185: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 26 :
                // InternalXhtml.g:1:201: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 27 :
                // InternalXhtml.g:1:217: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 28 :
                // InternalXhtml.g:1:225: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA16 dfa16 = new DFA16(this);
    static final String DFA16_eotS =
        "\1\uffff\1\33\1\35\1\36\1\37\1\40\1\41\1\42\1\43\1\44\1\45\2\uffff\1\52\6\uffff\4\32\41\uffff\1\103\4\uffff\1\103\5\uffff";
    static final String DFA16_eofS =
        "\104\uffff";
    static final String DFA16_minS =
        "\1\0\12\60\2\uffff\1\52\6\uffff\3\0\1\101\32\uffff\2\0\1\uffff\2\0\2\uffff\2\0\1\uffff\4\0\1\uffff\2\0\1\uffff";
    static final String DFA16_maxS =
        "\1\uffff\12\71\2\uffff\1\57\6\uffff\3\uffff\1\172\32\uffff\2\uffff\1\uffff\2\uffff\2\uffff\2\uffff\1\uffff\4\uffff\1\uffff\2\uffff\1\uffff";
    static final String DFA16_acceptS =
        "\13\uffff\1\13\1\14\1\uffff\1\16\1\17\1\20\1\21\1\22\1\23\4\uffff\1\26\1\33\1\34\1\1\1\27\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\1\14\1\31\1\32\1\15\1\16\1\17\1\20\1\21\1\22\1\23\1\24\2\uffff\1\30\2\uffff\1\26\1\33\2\uffff\1\25\4\uffff\1\25\2\uffff\1\25";
    static final String DFA16_specialS =
        "\1\0\23\uffff\1\4\1\17\1\5\33\uffff\1\1\1\7\1\uffff\1\15\1\2\2\uffff\1\16\1\13\1\uffff\1\12\1\14\1\11\1\6\1\uffff\1\3\1\10\1\uffff}>";
    static final String[] DFA16_transitionS = {
            "\11\32\2\31\2\32\1\31\22\32\1\31\1\32\1\25\1\16\1\20\1\24\1\32\1\26\1\13\1\14\4\32\1\17\1\15\1\12\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\1\23\1\21\1\32\1\22\3\32\32\30\3\32\1\27\1\30\1\32\32\30\uff85\32",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "\12\34",
            "",
            "",
            "\1\50\4\uffff\1\51",
            "",
            "",
            "",
            "",
            "",
            "",
            "\45\61\1\uffff\uffda\61",
            "\42\63\1\64\71\63\1\62\uffa3\63",
            "\47\66\1\64\64\66\1\65\uffa3\66",
            "\32\67\4\uffff\1\67\1\uffff\32\67",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\42\72\1\71\uffdd\72",
            "\42\75\1\73\71\75\1\74\uffa3\75",
            "",
            "\47\77\1\76\uffd8\77",
            "\47\102\1\100\64\102\1\101\uffa3\102",
            "",
            "",
            "\42\75\1\73\71\75\1\74\uffa3\75",
            "\42\75\1\73\71\75\1\74\uffa3\75",
            "",
            "\42\72\1\71\uffdd\72",
            "\42\75\1\73\71\75\1\74\uffa3\75",
            "\47\102\1\100\64\102\1\101\uffa3\102",
            "\47\102\1\100\64\102\1\101\uffa3\102",
            "",
            "\47\77\1\76\uffd8\77",
            "\47\102\1\100\64\102\1\101\uffa3\102",
            ""
    };

    static final short[] DFA16_eot = DFA.unpackEncodedString(DFA16_eotS);
    static final short[] DFA16_eof = DFA.unpackEncodedString(DFA16_eofS);
    static final char[] DFA16_min = DFA.unpackEncodedStringToUnsignedChars(DFA16_minS);
    static final char[] DFA16_max = DFA.unpackEncodedStringToUnsignedChars(DFA16_maxS);
    static final short[] DFA16_accept = DFA.unpackEncodedString(DFA16_acceptS);
    static final short[] DFA16_special = DFA.unpackEncodedString(DFA16_specialS);
    static final short[][] DFA16_transition;

    static {
        int numStates = DFA16_transitionS.length;
        DFA16_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA16_transition[i] = DFA.unpackEncodedString(DFA16_transitionS[i]);
        }
    }

    class DFA16 extends DFA {

        public DFA16(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 16;
            this.eot = DFA16_eot;
            this.eof = DFA16_eof;
            this.min = DFA16_min;
            this.max = DFA16_max;
            this.accept = DFA16_accept;
            this.special = DFA16_special;
            this.transition = DFA16_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | RULE_COMMENT_TEXT | RULE_STD_TEXT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA16_0 = input.LA(1);

                        s = -1;
                        if ( (LA16_0=='1') ) {s = 1;}

                        else if ( (LA16_0=='2') ) {s = 2;}

                        else if ( (LA16_0=='3') ) {s = 3;}

                        else if ( (LA16_0=='4') ) {s = 4;}

                        else if ( (LA16_0=='5') ) {s = 5;}

                        else if ( (LA16_0=='6') ) {s = 6;}

                        else if ( (LA16_0=='7') ) {s = 7;}

                        else if ( (LA16_0=='8') ) {s = 8;}

                        else if ( (LA16_0=='9') ) {s = 9;}

                        else if ( (LA16_0=='0') ) {s = 10;}

                        else if ( (LA16_0=='(') ) {s = 11;}

                        else if ( (LA16_0==')') ) {s = 12;}

                        else if ( (LA16_0=='/') ) {s = 13;}

                        else if ( (LA16_0=='#') ) {s = 14;}

                        else if ( (LA16_0=='.') ) {s = 15;}

                        else if ( (LA16_0=='$') ) {s = 16;}

                        else if ( (LA16_0==';') ) {s = 17;}

                        else if ( (LA16_0=='=') ) {s = 18;}

                        else if ( (LA16_0==':') ) {s = 19;}

                        else if ( (LA16_0=='%') ) {s = 20;}

                        else if ( (LA16_0=='\"') ) {s = 21;}

                        else if ( (LA16_0=='\'') ) {s = 22;}

                        else if ( (LA16_0=='^') ) {s = 23;}

                        else if ( ((LA16_0>='A' && LA16_0<='Z')||LA16_0=='_'||(LA16_0>='a' && LA16_0<='z')) ) {s = 24;}

                        else if ( ((LA16_0>='\t' && LA16_0<='\n')||LA16_0=='\r'||LA16_0==' ') ) {s = 25;}

                        else if ( ((LA16_0>='\u0000' && LA16_0<='\b')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\u001F')||LA16_0=='!'||LA16_0=='&'||(LA16_0>='*' && LA16_0<='-')||LA16_0=='<'||(LA16_0>='>' && LA16_0<='@')||(LA16_0>='[' && LA16_0<=']')||LA16_0=='`'||(LA16_0>='{' && LA16_0<='\uFFFF')) ) {s = 26;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA16_50 = input.LA(1);

                        s = -1;
                        if ( (LA16_50=='\"') ) {s = 57;}

                        else if ( ((LA16_50>='\u0000' && LA16_50<='!')||(LA16_50>='#' && LA16_50<='\uFFFF')) ) {s = 58;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA16_54 = input.LA(1);

                        s = -1;
                        if ( (LA16_54=='\'') ) {s = 64;}

                        else if ( (LA16_54=='\\') ) {s = 65;}

                        else if ( ((LA16_54>='\u0000' && LA16_54<='&')||(LA16_54>='(' && LA16_54<='[')||(LA16_54>=']' && LA16_54<='\uFFFF')) ) {s = 66;}

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA16_65 = input.LA(1);

                        s = -1;
                        if ( (LA16_65=='\'') ) {s = 62;}

                        else if ( ((LA16_65>='\u0000' && LA16_65<='&')||(LA16_65>='(' && LA16_65<='\uFFFF')) ) {s = 63;}

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA16_20 = input.LA(1);

                        s = -1;
                        if ( ((LA16_20>='\u0000' && LA16_20<='$')||(LA16_20>='&' && LA16_20<='\uFFFF')) ) {s = 49;}

                        else s = 26;

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA16_22 = input.LA(1);

                        s = -1;
                        if ( (LA16_22=='\\') ) {s = 53;}

                        else if ( ((LA16_22>='\u0000' && LA16_22<='&')||(LA16_22>='(' && LA16_22<='[')||(LA16_22>=']' && LA16_22<='\uFFFF')) ) {s = 54;}

                        else if ( (LA16_22=='\'') ) {s = 52;}

                        else s = 26;

                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA16_63 = input.LA(1);

                        s = -1;
                        if ( (LA16_63=='\'') ) {s = 64;}

                        else if ( (LA16_63=='\\') ) {s = 65;}

                        else if ( ((LA16_63>='\u0000' && LA16_63<='&')||(LA16_63>='(' && LA16_63<='[')||(LA16_63>=']' && LA16_63<='\uFFFF')) ) {s = 66;}

                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA16_51 = input.LA(1);

                        s = -1;
                        if ( (LA16_51=='\"') ) {s = 59;}

                        else if ( (LA16_51=='\\') ) {s = 60;}

                        else if ( ((LA16_51>='\u0000' && LA16_51<='!')||(LA16_51>='#' && LA16_51<='[')||(LA16_51>=']' && LA16_51<='\uFFFF')) ) {s = 61;}

                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA16_66 = input.LA(1);

                        s = -1;
                        if ( (LA16_66=='\'') ) {s = 64;}

                        else if ( (LA16_66=='\\') ) {s = 65;}

                        else if ( ((LA16_66>='\u0000' && LA16_66<='&')||(LA16_66>='(' && LA16_66<='[')||(LA16_66>=']' && LA16_66<='\uFFFF')) ) {s = 66;}

                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA16_62 = input.LA(1);

                        s = -1;
                        if ( (LA16_62=='\'') ) {s = 64;}

                        else if ( (LA16_62=='\\') ) {s = 65;}

                        else if ( ((LA16_62>='\u0000' && LA16_62<='&')||(LA16_62>='(' && LA16_62<='[')||(LA16_62>=']' && LA16_62<='\uFFFF')) ) {s = 66;}

                        else s = 67;

                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA16_60 = input.LA(1);

                        s = -1;
                        if ( (LA16_60=='\"') ) {s = 57;}

                        else if ( ((LA16_60>='\u0000' && LA16_60<='!')||(LA16_60>='#' && LA16_60<='\uFFFF')) ) {s = 58;}

                        if ( s>=0 ) return s;
                        break;
                    case 11 : 
                        int LA16_58 = input.LA(1);

                        s = -1;
                        if ( (LA16_58=='\"') ) {s = 59;}

                        else if ( (LA16_58=='\\') ) {s = 60;}

                        else if ( ((LA16_58>='\u0000' && LA16_58<='!')||(LA16_58>='#' && LA16_58<='[')||(LA16_58>=']' && LA16_58<='\uFFFF')) ) {s = 61;}

                        if ( s>=0 ) return s;
                        break;
                    case 12 : 
                        int LA16_61 = input.LA(1);

                        s = -1;
                        if ( (LA16_61=='\"') ) {s = 59;}

                        else if ( (LA16_61=='\\') ) {s = 60;}

                        else if ( ((LA16_61>='\u0000' && LA16_61<='!')||(LA16_61>='#' && LA16_61<='[')||(LA16_61>=']' && LA16_61<='\uFFFF')) ) {s = 61;}

                        if ( s>=0 ) return s;
                        break;
                    case 13 : 
                        int LA16_53 = input.LA(1);

                        s = -1;
                        if ( (LA16_53=='\'') ) {s = 62;}

                        else if ( ((LA16_53>='\u0000' && LA16_53<='&')||(LA16_53>='(' && LA16_53<='\uFFFF')) ) {s = 63;}

                        if ( s>=0 ) return s;
                        break;
                    case 14 : 
                        int LA16_57 = input.LA(1);

                        s = -1;
                        if ( (LA16_57=='\"') ) {s = 59;}

                        else if ( (LA16_57=='\\') ) {s = 60;}

                        else if ( ((LA16_57>='\u0000' && LA16_57<='!')||(LA16_57>='#' && LA16_57<='[')||(LA16_57>=']' && LA16_57<='\uFFFF')) ) {s = 61;}

                        else s = 67;

                        if ( s>=0 ) return s;
                        break;
                    case 15 : 
                        int LA16_21 = input.LA(1);

                        s = -1;
                        if ( (LA16_21=='\\') ) {s = 50;}

                        else if ( ((LA16_21>='\u0000' && LA16_21<='!')||(LA16_21>='#' && LA16_21<='[')||(LA16_21>=']' && LA16_21<='\uFFFF')) ) {s = 51;}

                        else if ( (LA16_21=='\"') ) {s = 52;}

                        else s = 26;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 16, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}