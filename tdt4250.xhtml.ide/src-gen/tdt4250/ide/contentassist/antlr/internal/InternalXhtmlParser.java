package tdt4250.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import tdt4250.services.XhtmlGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXhtmlParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STD_TEXT", "RULE_ID", "RULE_COMMENT_TEXT", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'1'", "'2'", "'3'", "'4'", "'5'", "'6'", "'7'", "'8'", "'9'", "'0'", "'('", "')'", "'/'", "'#'", "'.'", "'$'", "';'", "'='", "':'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_STD_TEXT=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int RULE_COMMENT_TEXT=6;
    public static final int RULE_ID=5;
    public static final int RULE_WS=11;
    public static final int RULE_ANY_OTHER=12;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalXhtmlParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXhtmlParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXhtmlParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXhtml.g"; }


    	private XhtmlGrammarAccess grammarAccess;

    	public void setGrammarAccess(XhtmlGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDomainmodel"
    // InternalXhtml.g:53:1: entryRuleDomainmodel : ruleDomainmodel EOF ;
    public final void entryRuleDomainmodel() throws RecognitionException {
        try {
            // InternalXhtml.g:54:1: ( ruleDomainmodel EOF )
            // InternalXhtml.g:55:1: ruleDomainmodel EOF
            {
             before(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_1);
            ruleDomainmodel();

            state._fsp--;

             after(grammarAccess.getDomainmodelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // InternalXhtml.g:62:1: ruleDomainmodel : ( ( rule__Domainmodel__ElementsAssignment )* ) ;
    public final void ruleDomainmodel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:66:2: ( ( ( rule__Domainmodel__ElementsAssignment )* ) )
            // InternalXhtml.g:67:2: ( ( rule__Domainmodel__ElementsAssignment )* )
            {
            // InternalXhtml.g:67:2: ( ( rule__Domainmodel__ElementsAssignment )* )
            // InternalXhtml.g:68:3: ( rule__Domainmodel__ElementsAssignment )*
            {
             before(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 
            // InternalXhtml.g:69:3: ( rule__Domainmodel__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=RULE_STD_TEXT && LA1_0<=RULE_COMMENT_TEXT)||LA1_0==25||LA1_0==28) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXhtml.g:69:4: rule__Domainmodel__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Domainmodel__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleType"
    // InternalXhtml.g:78:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalXhtml.g:79:1: ( ruleType EOF )
            // InternalXhtml.g:80:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalXhtml.g:87:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:91:2: ( ( ( rule__Type__Alternatives ) ) )
            // InternalXhtml.g:92:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalXhtml.g:92:2: ( ( rule__Type__Alternatives ) )
            // InternalXhtml.g:93:3: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalXhtml.g:94:3: ( rule__Type__Alternatives )
            // InternalXhtml.g:94:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleComment"
    // InternalXhtml.g:103:1: entryRuleComment : ruleComment EOF ;
    public final void entryRuleComment() throws RecognitionException {
        try {
            // InternalXhtml.g:104:1: ( ruleComment EOF )
            // InternalXhtml.g:105:1: ruleComment EOF
            {
             before(grammarAccess.getCommentRule()); 
            pushFollow(FOLLOW_1);
            ruleComment();

            state._fsp--;

             after(grammarAccess.getCommentRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComment"


    // $ANTLR start "ruleComment"
    // InternalXhtml.g:112:1: ruleComment : ( ( rule__Comment__Group__0 ) ) ;
    public final void ruleComment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:116:2: ( ( ( rule__Comment__Group__0 ) ) )
            // InternalXhtml.g:117:2: ( ( rule__Comment__Group__0 ) )
            {
            // InternalXhtml.g:117:2: ( ( rule__Comment__Group__0 ) )
            // InternalXhtml.g:118:3: ( rule__Comment__Group__0 )
            {
             before(grammarAccess.getCommentAccess().getGroup()); 
            // InternalXhtml.g:119:3: ( rule__Comment__Group__0 )
            // InternalXhtml.g:119:4: rule__Comment__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Comment__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCommentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComment"


    // $ANTLR start "entryRuleXmlElement"
    // InternalXhtml.g:128:1: entryRuleXmlElement : ruleXmlElement EOF ;
    public final void entryRuleXmlElement() throws RecognitionException {
        try {
            // InternalXhtml.g:129:1: ( ruleXmlElement EOF )
            // InternalXhtml.g:130:1: ruleXmlElement EOF
            {
             before(grammarAccess.getXmlElementRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlElement();

            state._fsp--;

             after(grammarAccess.getXmlElementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlElement"


    // $ANTLR start "ruleXmlElement"
    // InternalXhtml.g:137:1: ruleXmlElement : ( ( rule__XmlElement__Group__0 ) ) ;
    public final void ruleXmlElement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:141:2: ( ( ( rule__XmlElement__Group__0 ) ) )
            // InternalXhtml.g:142:2: ( ( rule__XmlElement__Group__0 ) )
            {
            // InternalXhtml.g:142:2: ( ( rule__XmlElement__Group__0 ) )
            // InternalXhtml.g:143:3: ( rule__XmlElement__Group__0 )
            {
             before(grammarAccess.getXmlElementAccess().getGroup()); 
            // InternalXhtml.g:144:3: ( rule__XmlElement__Group__0 )
            // InternalXhtml.g:144:4: rule__XmlElement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlElementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlElement"


    // $ANTLR start "entryRuleInnerText"
    // InternalXhtml.g:153:1: entryRuleInnerText : ruleInnerText EOF ;
    public final void entryRuleInnerText() throws RecognitionException {
        try {
            // InternalXhtml.g:154:1: ( ruleInnerText EOF )
            // InternalXhtml.g:155:1: ruleInnerText EOF
            {
             before(grammarAccess.getInnerTextRule()); 
            pushFollow(FOLLOW_1);
            ruleInnerText();

            state._fsp--;

             after(grammarAccess.getInnerTextRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInnerText"


    // $ANTLR start "ruleInnerText"
    // InternalXhtml.g:162:1: ruleInnerText : ( ( rule__InnerText__Group__0 ) ) ;
    public final void ruleInnerText() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:166:2: ( ( ( rule__InnerText__Group__0 ) ) )
            // InternalXhtml.g:167:2: ( ( rule__InnerText__Group__0 ) )
            {
            // InternalXhtml.g:167:2: ( ( rule__InnerText__Group__0 ) )
            // InternalXhtml.g:168:3: ( rule__InnerText__Group__0 )
            {
             before(grammarAccess.getInnerTextAccess().getGroup()); 
            // InternalXhtml.g:169:3: ( rule__InnerText__Group__0 )
            // InternalXhtml.g:169:4: rule__InnerText__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InnerText__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInnerTextAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInnerText"


    // $ANTLR start "entryRuleVariable"
    // InternalXhtml.g:178:1: entryRuleVariable : ruleVariable EOF ;
    public final void entryRuleVariable() throws RecognitionException {
        try {
            // InternalXhtml.g:179:1: ( ruleVariable EOF )
            // InternalXhtml.g:180:1: ruleVariable EOF
            {
             before(grammarAccess.getVariableRule()); 
            pushFollow(FOLLOW_1);
            ruleVariable();

            state._fsp--;

             after(grammarAccess.getVariableRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalXhtml.g:187:1: ruleVariable : ( ( rule__Variable__Group__0 ) ) ;
    public final void ruleVariable() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:191:2: ( ( ( rule__Variable__Group__0 ) ) )
            // InternalXhtml.g:192:2: ( ( rule__Variable__Group__0 ) )
            {
            // InternalXhtml.g:192:2: ( ( rule__Variable__Group__0 ) )
            // InternalXhtml.g:193:3: ( rule__Variable__Group__0 )
            {
             before(grammarAccess.getVariableAccess().getGroup()); 
            // InternalXhtml.g:194:3: ( rule__Variable__Group__0 )
            // InternalXhtml.g:194:4: rule__Variable__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleXmlAttribute"
    // InternalXhtml.g:203:1: entryRuleXmlAttribute : ruleXmlAttribute EOF ;
    public final void entryRuleXmlAttribute() throws RecognitionException {
        try {
            // InternalXhtml.g:204:1: ( ruleXmlAttribute EOF )
            // InternalXhtml.g:205:1: ruleXmlAttribute EOF
            {
             before(grammarAccess.getXmlAttributeRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlAttribute();

            state._fsp--;

             after(grammarAccess.getXmlAttributeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlAttribute"


    // $ANTLR start "ruleXmlAttribute"
    // InternalXhtml.g:212:1: ruleXmlAttribute : ( ( rule__XmlAttribute__Group__0 ) ) ;
    public final void ruleXmlAttribute() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:216:2: ( ( ( rule__XmlAttribute__Group__0 ) ) )
            // InternalXhtml.g:217:2: ( ( rule__XmlAttribute__Group__0 ) )
            {
            // InternalXhtml.g:217:2: ( ( rule__XmlAttribute__Group__0 ) )
            // InternalXhtml.g:218:3: ( rule__XmlAttribute__Group__0 )
            {
             before(grammarAccess.getXmlAttributeAccess().getGroup()); 
            // InternalXhtml.g:219:3: ( rule__XmlAttribute__Group__0 )
            // InternalXhtml.g:219:4: rule__XmlAttribute__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlAttribute"


    // $ANTLR start "entryRuleXmlStyle"
    // InternalXhtml.g:228:1: entryRuleXmlStyle : ruleXmlStyle EOF ;
    public final void entryRuleXmlStyle() throws RecognitionException {
        try {
            // InternalXhtml.g:229:1: ( ruleXmlStyle EOF )
            // InternalXhtml.g:230:1: ruleXmlStyle EOF
            {
             before(grammarAccess.getXmlStyleRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlStyle();

            state._fsp--;

             after(grammarAccess.getXmlStyleRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlStyle"


    // $ANTLR start "ruleXmlStyle"
    // InternalXhtml.g:237:1: ruleXmlStyle : ( ( rule__XmlStyle__Group__0 ) ) ;
    public final void ruleXmlStyle() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:241:2: ( ( ( rule__XmlStyle__Group__0 ) ) )
            // InternalXhtml.g:242:2: ( ( rule__XmlStyle__Group__0 ) )
            {
            // InternalXhtml.g:242:2: ( ( rule__XmlStyle__Group__0 ) )
            // InternalXhtml.g:243:3: ( rule__XmlStyle__Group__0 )
            {
             before(grammarAccess.getXmlStyleAccess().getGroup()); 
            // InternalXhtml.g:244:3: ( rule__XmlStyle__Group__0 )
            // InternalXhtml.g:244:4: rule__XmlStyle__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlStyle__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlStyleAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlStyle"


    // $ANTLR start "entryRuleXmlInt"
    // InternalXhtml.g:253:1: entryRuleXmlInt : ruleXmlInt EOF ;
    public final void entryRuleXmlInt() throws RecognitionException {
        try {
            // InternalXhtml.g:254:1: ( ruleXmlInt EOF )
            // InternalXhtml.g:255:1: ruleXmlInt EOF
            {
             before(grammarAccess.getXmlIntRule()); 
            pushFollow(FOLLOW_1);
            ruleXmlInt();

            state._fsp--;

             after(grammarAccess.getXmlIntRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXmlInt"


    // $ANTLR start "ruleXmlInt"
    // InternalXhtml.g:262:1: ruleXmlInt : ( ( rule__XmlInt__Group__0 ) ) ;
    public final void ruleXmlInt() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:266:2: ( ( ( rule__XmlInt__Group__0 ) ) )
            // InternalXhtml.g:267:2: ( ( rule__XmlInt__Group__0 ) )
            {
            // InternalXhtml.g:267:2: ( ( rule__XmlInt__Group__0 ) )
            // InternalXhtml.g:268:3: ( rule__XmlInt__Group__0 )
            {
             before(grammarAccess.getXmlIntAccess().getGroup()); 
            // InternalXhtml.g:269:3: ( rule__XmlInt__Group__0 )
            // InternalXhtml.g:269:4: rule__XmlInt__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XmlInt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXmlIntAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXmlInt"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalXhtml.g:277:1: rule__Type__Alternatives : ( ( ruleComment ) | ( ruleXmlElement ) | ( ruleInnerText ) | ( ruleVariable ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:281:1: ( ( ruleComment ) | ( ruleXmlElement ) | ( ruleInnerText ) | ( ruleVariable ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case RULE_COMMENT_TEXT:
                {
                alt2=1;
                }
                break;
            case 25:
                {
                alt2=2;
                }
                break;
            case RULE_ID:
                {
                switch ( input.LA(2) ) {
                case EOF:
                case RULE_COMMENT_TEXT:
                case 24:
                case 25:
                case 28:
                    {
                    alt2=3;
                    }
                    break;
                case RULE_ID:
                    {
                    int LA2_6 = input.LA(3);

                    if ( (LA2_6==EOF||(LA2_6>=RULE_STD_TEXT && LA2_6<=RULE_COMMENT_TEXT)||(LA2_6>=23 && LA2_6<=28)) ) {
                        alt2=3;
                    }
                    else if ( ((LA2_6>=30 && LA2_6<=31)) ) {
                        alt2=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 6, input);

                        throw nvae;
                    }
                    }
                    break;
                case RULE_STD_TEXT:
                    {
                    int LA2_7 = input.LA(3);

                    if ( (LA2_7==31) ) {
                        alt2=2;
                    }
                    else if ( (LA2_7==EOF||(LA2_7>=RULE_STD_TEXT && LA2_7<=RULE_COMMENT_TEXT)||(LA2_7>=24 && LA2_7<=25)||LA2_7==28) ) {
                        alt2=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 7, input);

                        throw nvae;
                    }
                    }
                    break;
                case 23:
                case 26:
                case 27:
                    {
                    alt2=2;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 3, input);

                    throw nvae;
                }

                }
                break;
            case RULE_STD_TEXT:
                {
                alt2=3;
                }
                break;
            case 28:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalXhtml.g:282:2: ( ruleComment )
                    {
                    // InternalXhtml.g:282:2: ( ruleComment )
                    // InternalXhtml.g:283:3: ruleComment
                    {
                     before(grammarAccess.getTypeAccess().getCommentParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleComment();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getCommentParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:288:2: ( ruleXmlElement )
                    {
                    // InternalXhtml.g:288:2: ( ruleXmlElement )
                    // InternalXhtml.g:289:3: ruleXmlElement
                    {
                     before(grammarAccess.getTypeAccess().getXmlElementParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlElement();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getXmlElementParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXhtml.g:294:2: ( ruleInnerText )
                    {
                    // InternalXhtml.g:294:2: ( ruleInnerText )
                    // InternalXhtml.g:295:3: ruleInnerText
                    {
                     before(grammarAccess.getTypeAccess().getInnerTextParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleInnerText();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getInnerTextParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXhtml.g:300:2: ( ruleVariable )
                    {
                    // InternalXhtml.g:300:2: ( ruleVariable )
                    // InternalXhtml.g:301:3: ruleVariable
                    {
                     before(grammarAccess.getTypeAccess().getVariableParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleVariable();

                    state._fsp--;

                     after(grammarAccess.getTypeAccess().getVariableParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__XmlElement__Alternatives_0"
    // InternalXhtml.g:310:1: rule__XmlElement__Alternatives_0 : ( ( ( rule__XmlElement__Group_0_0__0 ) ) | ( ( rule__XmlElement__TypeAssignment_0_1 ) ) );
    public final void rule__XmlElement__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:314:1: ( ( ( rule__XmlElement__Group_0_0__0 ) ) | ( ( rule__XmlElement__TypeAssignment_0_1 ) ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==25) ) {
                alt3=1;
            }
            else if ( (LA3_0==RULE_ID) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalXhtml.g:315:2: ( ( rule__XmlElement__Group_0_0__0 ) )
                    {
                    // InternalXhtml.g:315:2: ( ( rule__XmlElement__Group_0_0__0 ) )
                    // InternalXhtml.g:316:3: ( rule__XmlElement__Group_0_0__0 )
                    {
                     before(grammarAccess.getXmlElementAccess().getGroup_0_0()); 
                    // InternalXhtml.g:317:3: ( rule__XmlElement__Group_0_0__0 )
                    // InternalXhtml.g:317:4: rule__XmlElement__Group_0_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__XmlElement__Group_0_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getXmlElementAccess().getGroup_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:321:2: ( ( rule__XmlElement__TypeAssignment_0_1 ) )
                    {
                    // InternalXhtml.g:321:2: ( ( rule__XmlElement__TypeAssignment_0_1 ) )
                    // InternalXhtml.g:322:3: ( rule__XmlElement__TypeAssignment_0_1 )
                    {
                     before(grammarAccess.getXmlElementAccess().getTypeAssignment_0_1()); 
                    // InternalXhtml.g:323:3: ( rule__XmlElement__TypeAssignment_0_1 )
                    // InternalXhtml.g:323:4: rule__XmlElement__TypeAssignment_0_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__XmlElement__TypeAssignment_0_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getXmlElementAccess().getTypeAssignment_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Alternatives_0"


    // $ANTLR start "rule__InnerText__TextAlternatives_1_0"
    // InternalXhtml.g:331:1: rule__InnerText__TextAlternatives_1_0 : ( ( RULE_STD_TEXT ) | ( RULE_ID ) );
    public final void rule__InnerText__TextAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:335:1: ( ( RULE_STD_TEXT ) | ( RULE_ID ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==RULE_STD_TEXT) ) {
                alt4=1;
            }
            else if ( (LA4_0==RULE_ID) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalXhtml.g:336:2: ( RULE_STD_TEXT )
                    {
                    // InternalXhtml.g:336:2: ( RULE_STD_TEXT )
                    // InternalXhtml.g:337:3: RULE_STD_TEXT
                    {
                     before(grammarAccess.getInnerTextAccess().getTextSTD_TEXTTerminalRuleCall_1_0_0()); 
                    match(input,RULE_STD_TEXT,FOLLOW_2); 
                     after(grammarAccess.getInnerTextAccess().getTextSTD_TEXTTerminalRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:342:2: ( RULE_ID )
                    {
                    // InternalXhtml.g:342:2: ( RULE_ID )
                    // InternalXhtml.g:343:3: RULE_ID
                    {
                     before(grammarAccess.getInnerTextAccess().getTextIDTerminalRuleCall_1_0_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getInnerTextAccess().getTextIDTerminalRuleCall_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InnerText__TextAlternatives_1_0"


    // $ANTLR start "rule__Variable__ValueAlternatives_2_1_0"
    // InternalXhtml.g:352:1: rule__Variable__ValueAlternatives_2_1_0 : ( ( RULE_ID ) | ( RULE_STD_TEXT ) | ( ruleXmlInt ) );
    public final void rule__Variable__ValueAlternatives_2_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:356:1: ( ( RULE_ID ) | ( RULE_STD_TEXT ) | ( ruleXmlInt ) )
            int alt5=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt5=1;
                }
                break;
            case RULE_STD_TEXT:
                {
                alt5=2;
                }
                break;
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalXhtml.g:357:2: ( RULE_ID )
                    {
                    // InternalXhtml.g:357:2: ( RULE_ID )
                    // InternalXhtml.g:358:3: RULE_ID
                    {
                     before(grammarAccess.getVariableAccess().getValueIDTerminalRuleCall_2_1_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getVariableAccess().getValueIDTerminalRuleCall_2_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:363:2: ( RULE_STD_TEXT )
                    {
                    // InternalXhtml.g:363:2: ( RULE_STD_TEXT )
                    // InternalXhtml.g:364:3: RULE_STD_TEXT
                    {
                     before(grammarAccess.getVariableAccess().getValueSTD_TEXTTerminalRuleCall_2_1_0_1()); 
                    match(input,RULE_STD_TEXT,FOLLOW_2); 
                     after(grammarAccess.getVariableAccess().getValueSTD_TEXTTerminalRuleCall_2_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXhtml.g:369:2: ( ruleXmlInt )
                    {
                    // InternalXhtml.g:369:2: ( ruleXmlInt )
                    // InternalXhtml.g:370:3: ruleXmlInt
                    {
                     before(grammarAccess.getVariableAccess().getValueXmlIntParserRuleCall_2_1_0_2()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlInt();

                    state._fsp--;

                     after(grammarAccess.getVariableAccess().getValueXmlIntParserRuleCall_2_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__ValueAlternatives_2_1_0"


    // $ANTLR start "rule__XmlAttribute__ValueAlternatives_2_0"
    // InternalXhtml.g:379:1: rule__XmlAttribute__ValueAlternatives_2_0 : ( ( RULE_ID ) | ( RULE_STD_TEXT ) | ( ruleXmlInt ) );
    public final void rule__XmlAttribute__ValueAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:383:1: ( ( RULE_ID ) | ( RULE_STD_TEXT ) | ( ruleXmlInt ) )
            int alt6=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt6=1;
                }
                break;
            case RULE_STD_TEXT:
                {
                alt6=2;
                }
                break;
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                {
                alt6=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalXhtml.g:384:2: ( RULE_ID )
                    {
                    // InternalXhtml.g:384:2: ( RULE_ID )
                    // InternalXhtml.g:385:3: RULE_ID
                    {
                     before(grammarAccess.getXmlAttributeAccess().getValueIDTerminalRuleCall_2_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getXmlAttributeAccess().getValueIDTerminalRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:390:2: ( RULE_STD_TEXT )
                    {
                    // InternalXhtml.g:390:2: ( RULE_STD_TEXT )
                    // InternalXhtml.g:391:3: RULE_STD_TEXT
                    {
                     before(grammarAccess.getXmlAttributeAccess().getValueSTD_TEXTTerminalRuleCall_2_0_1()); 
                    match(input,RULE_STD_TEXT,FOLLOW_2); 
                     after(grammarAccess.getXmlAttributeAccess().getValueSTD_TEXTTerminalRuleCall_2_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXhtml.g:396:2: ( ruleXmlInt )
                    {
                    // InternalXhtml.g:396:2: ( ruleXmlInt )
                    // InternalXhtml.g:397:3: ruleXmlInt
                    {
                     before(grammarAccess.getXmlAttributeAccess().getValueXmlIntParserRuleCall_2_0_2()); 
                    pushFollow(FOLLOW_2);
                    ruleXmlInt();

                    state._fsp--;

                     after(grammarAccess.getXmlAttributeAccess().getValueXmlIntParserRuleCall_2_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__ValueAlternatives_2_0"


    // $ANTLR start "rule__XmlStyle__AttributeAlternatives_0_0"
    // InternalXhtml.g:406:1: rule__XmlStyle__AttributeAlternatives_0_0 : ( ( RULE_ID ) | ( RULE_STD_TEXT ) );
    public final void rule__XmlStyle__AttributeAlternatives_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:410:1: ( ( RULE_ID ) | ( RULE_STD_TEXT ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_STD_TEXT) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalXhtml.g:411:2: ( RULE_ID )
                    {
                    // InternalXhtml.g:411:2: ( RULE_ID )
                    // InternalXhtml.g:412:3: RULE_ID
                    {
                     before(grammarAccess.getXmlStyleAccess().getAttributeIDTerminalRuleCall_0_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getXmlStyleAccess().getAttributeIDTerminalRuleCall_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:417:2: ( RULE_STD_TEXT )
                    {
                    // InternalXhtml.g:417:2: ( RULE_STD_TEXT )
                    // InternalXhtml.g:418:3: RULE_STD_TEXT
                    {
                     before(grammarAccess.getXmlStyleAccess().getAttributeSTD_TEXTTerminalRuleCall_0_0_1()); 
                    match(input,RULE_STD_TEXT,FOLLOW_2); 
                     after(grammarAccess.getXmlStyleAccess().getAttributeSTD_TEXTTerminalRuleCall_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__AttributeAlternatives_0_0"


    // $ANTLR start "rule__XmlStyle__ValueAlternatives_2_0"
    // InternalXhtml.g:427:1: rule__XmlStyle__ValueAlternatives_2_0 : ( ( RULE_ID ) | ( RULE_STD_TEXT ) );
    public final void rule__XmlStyle__ValueAlternatives_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:431:1: ( ( RULE_ID ) | ( RULE_STD_TEXT ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_ID) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_STD_TEXT) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalXhtml.g:432:2: ( RULE_ID )
                    {
                    // InternalXhtml.g:432:2: ( RULE_ID )
                    // InternalXhtml.g:433:3: RULE_ID
                    {
                     before(grammarAccess.getXmlStyleAccess().getValueIDTerminalRuleCall_2_0_0()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getXmlStyleAccess().getValueIDTerminalRuleCall_2_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:438:2: ( RULE_STD_TEXT )
                    {
                    // InternalXhtml.g:438:2: ( RULE_STD_TEXT )
                    // InternalXhtml.g:439:3: RULE_STD_TEXT
                    {
                     before(grammarAccess.getXmlStyleAccess().getValueSTD_TEXTTerminalRuleCall_2_0_1()); 
                    match(input,RULE_STD_TEXT,FOLLOW_2); 
                     after(grammarAccess.getXmlStyleAccess().getValueSTD_TEXTTerminalRuleCall_2_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__ValueAlternatives_2_0"


    // $ANTLR start "rule__XmlInt__Alternatives_0"
    // InternalXhtml.g:448:1: rule__XmlInt__Alternatives_0 : ( ( '1' ) | ( '2' ) | ( '3' ) | ( '4' ) | ( '5' ) | ( '6' ) | ( '7' ) | ( '8' ) | ( '9' ) );
    public final void rule__XmlInt__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:452:1: ( ( '1' ) | ( '2' ) | ( '3' ) | ( '4' ) | ( '5' ) | ( '6' ) | ( '7' ) | ( '8' ) | ( '9' ) )
            int alt9=9;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt9=1;
                }
                break;
            case 14:
                {
                alt9=2;
                }
                break;
            case 15:
                {
                alt9=3;
                }
                break;
            case 16:
                {
                alt9=4;
                }
                break;
            case 17:
                {
                alt9=5;
                }
                break;
            case 18:
                {
                alt9=6;
                }
                break;
            case 19:
                {
                alt9=7;
                }
                break;
            case 20:
                {
                alt9=8;
                }
                break;
            case 21:
                {
                alt9=9;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalXhtml.g:453:2: ( '1' )
                    {
                    // InternalXhtml.g:453:2: ( '1' )
                    // InternalXhtml.g:454:3: '1'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitOneKeyword_0_0()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitOneKeyword_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:459:2: ( '2' )
                    {
                    // InternalXhtml.g:459:2: ( '2' )
                    // InternalXhtml.g:460:3: '2'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitTwoKeyword_0_1()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitTwoKeyword_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXhtml.g:465:2: ( '3' )
                    {
                    // InternalXhtml.g:465:2: ( '3' )
                    // InternalXhtml.g:466:3: '3'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitThreeKeyword_0_2()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitThreeKeyword_0_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXhtml.g:471:2: ( '4' )
                    {
                    // InternalXhtml.g:471:2: ( '4' )
                    // InternalXhtml.g:472:3: '4'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitFourKeyword_0_3()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitFourKeyword_0_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalXhtml.g:477:2: ( '5' )
                    {
                    // InternalXhtml.g:477:2: ( '5' )
                    // InternalXhtml.g:478:3: '5'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitFiveKeyword_0_4()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitFiveKeyword_0_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalXhtml.g:483:2: ( '6' )
                    {
                    // InternalXhtml.g:483:2: ( '6' )
                    // InternalXhtml.g:484:3: '6'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitSixKeyword_0_5()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitSixKeyword_0_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalXhtml.g:489:2: ( '7' )
                    {
                    // InternalXhtml.g:489:2: ( '7' )
                    // InternalXhtml.g:490:3: '7'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitSevenKeyword_0_6()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitSevenKeyword_0_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalXhtml.g:495:2: ( '8' )
                    {
                    // InternalXhtml.g:495:2: ( '8' )
                    // InternalXhtml.g:496:3: '8'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitEightKeyword_0_7()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitEightKeyword_0_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalXhtml.g:501:2: ( '9' )
                    {
                    // InternalXhtml.g:501:2: ( '9' )
                    // InternalXhtml.g:502:3: '9'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitNineKeyword_0_8()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitNineKeyword_0_8()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlInt__Alternatives_0"


    // $ANTLR start "rule__XmlInt__Alternatives_1"
    // InternalXhtml.g:511:1: rule__XmlInt__Alternatives_1 : ( ( '0' ) | ( '1' ) | ( '2' ) | ( '3' ) | ( '4' ) | ( '5' ) | ( '6' ) | ( '7' ) | ( '8' ) | ( '9' ) );
    public final void rule__XmlInt__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:515:1: ( ( '0' ) | ( '1' ) | ( '2' ) | ( '3' ) | ( '4' ) | ( '5' ) | ( '6' ) | ( '7' ) | ( '8' ) | ( '9' ) )
            int alt10=10;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt10=1;
                }
                break;
            case 13:
                {
                alt10=2;
                }
                break;
            case 14:
                {
                alt10=3;
                }
                break;
            case 15:
                {
                alt10=4;
                }
                break;
            case 16:
                {
                alt10=5;
                }
                break;
            case 17:
                {
                alt10=6;
                }
                break;
            case 18:
                {
                alt10=7;
                }
                break;
            case 19:
                {
                alt10=8;
                }
                break;
            case 20:
                {
                alt10=9;
                }
                break;
            case 21:
                {
                alt10=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalXhtml.g:516:2: ( '0' )
                    {
                    // InternalXhtml.g:516:2: ( '0' )
                    // InternalXhtml.g:517:3: '0'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitZeroKeyword_1_0()); 
                    match(input,22,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitZeroKeyword_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalXhtml.g:522:2: ( '1' )
                    {
                    // InternalXhtml.g:522:2: ( '1' )
                    // InternalXhtml.g:523:3: '1'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitOneKeyword_1_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitOneKeyword_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalXhtml.g:528:2: ( '2' )
                    {
                    // InternalXhtml.g:528:2: ( '2' )
                    // InternalXhtml.g:529:3: '2'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitTwoKeyword_1_2()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitTwoKeyword_1_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalXhtml.g:534:2: ( '3' )
                    {
                    // InternalXhtml.g:534:2: ( '3' )
                    // InternalXhtml.g:535:3: '3'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitThreeKeyword_1_3()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitThreeKeyword_1_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalXhtml.g:540:2: ( '4' )
                    {
                    // InternalXhtml.g:540:2: ( '4' )
                    // InternalXhtml.g:541:3: '4'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitFourKeyword_1_4()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitFourKeyword_1_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalXhtml.g:546:2: ( '5' )
                    {
                    // InternalXhtml.g:546:2: ( '5' )
                    // InternalXhtml.g:547:3: '5'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitFiveKeyword_1_5()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitFiveKeyword_1_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalXhtml.g:552:2: ( '6' )
                    {
                    // InternalXhtml.g:552:2: ( '6' )
                    // InternalXhtml.g:553:3: '6'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitSixKeyword_1_6()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitSixKeyword_1_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalXhtml.g:558:2: ( '7' )
                    {
                    // InternalXhtml.g:558:2: ( '7' )
                    // InternalXhtml.g:559:3: '7'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitSevenKeyword_1_7()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitSevenKeyword_1_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalXhtml.g:564:2: ( '8' )
                    {
                    // InternalXhtml.g:564:2: ( '8' )
                    // InternalXhtml.g:565:3: '8'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitEightKeyword_1_8()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitEightKeyword_1_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalXhtml.g:570:2: ( '9' )
                    {
                    // InternalXhtml.g:570:2: ( '9' )
                    // InternalXhtml.g:571:3: '9'
                    {
                     before(grammarAccess.getXmlIntAccess().getDigitNineKeyword_1_9()); 
                    match(input,21,FOLLOW_2); 
                     after(grammarAccess.getXmlIntAccess().getDigitNineKeyword_1_9()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlInt__Alternatives_1"


    // $ANTLR start "rule__Comment__Group__0"
    // InternalXhtml.g:580:1: rule__Comment__Group__0 : rule__Comment__Group__0__Impl rule__Comment__Group__1 ;
    public final void rule__Comment__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:584:1: ( rule__Comment__Group__0__Impl rule__Comment__Group__1 )
            // InternalXhtml.g:585:2: rule__Comment__Group__0__Impl rule__Comment__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__Comment__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Comment__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__0"


    // $ANTLR start "rule__Comment__Group__0__Impl"
    // InternalXhtml.g:592:1: rule__Comment__Group__0__Impl : ( () ) ;
    public final void rule__Comment__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:596:1: ( ( () ) )
            // InternalXhtml.g:597:1: ( () )
            {
            // InternalXhtml.g:597:1: ( () )
            // InternalXhtml.g:598:2: ()
            {
             before(grammarAccess.getCommentAccess().getCommentAction_0()); 
            // InternalXhtml.g:599:2: ()
            // InternalXhtml.g:599:3: 
            {
            }

             after(grammarAccess.getCommentAccess().getCommentAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__0__Impl"


    // $ANTLR start "rule__Comment__Group__1"
    // InternalXhtml.g:607:1: rule__Comment__Group__1 : rule__Comment__Group__1__Impl ;
    public final void rule__Comment__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:611:1: ( rule__Comment__Group__1__Impl )
            // InternalXhtml.g:612:2: rule__Comment__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Comment__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__1"


    // $ANTLR start "rule__Comment__Group__1__Impl"
    // InternalXhtml.g:618:1: rule__Comment__Group__1__Impl : ( ( rule__Comment__TextAssignment_1 ) ) ;
    public final void rule__Comment__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:622:1: ( ( ( rule__Comment__TextAssignment_1 ) ) )
            // InternalXhtml.g:623:1: ( ( rule__Comment__TextAssignment_1 ) )
            {
            // InternalXhtml.g:623:1: ( ( rule__Comment__TextAssignment_1 ) )
            // InternalXhtml.g:624:2: ( rule__Comment__TextAssignment_1 )
            {
             before(grammarAccess.getCommentAccess().getTextAssignment_1()); 
            // InternalXhtml.g:625:2: ( rule__Comment__TextAssignment_1 )
            // InternalXhtml.g:625:3: rule__Comment__TextAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Comment__TextAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCommentAccess().getTextAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__Group__1__Impl"


    // $ANTLR start "rule__XmlElement__Group__0"
    // InternalXhtml.g:634:1: rule__XmlElement__Group__0 : rule__XmlElement__Group__0__Impl rule__XmlElement__Group__1 ;
    public final void rule__XmlElement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:638:1: ( rule__XmlElement__Group__0__Impl rule__XmlElement__Group__1 )
            // InternalXhtml.g:639:2: rule__XmlElement__Group__0__Impl rule__XmlElement__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__XmlElement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__0"


    // $ANTLR start "rule__XmlElement__Group__0__Impl"
    // InternalXhtml.g:646:1: rule__XmlElement__Group__0__Impl : ( ( rule__XmlElement__Alternatives_0 ) ) ;
    public final void rule__XmlElement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:650:1: ( ( ( rule__XmlElement__Alternatives_0 ) ) )
            // InternalXhtml.g:651:1: ( ( rule__XmlElement__Alternatives_0 ) )
            {
            // InternalXhtml.g:651:1: ( ( rule__XmlElement__Alternatives_0 ) )
            // InternalXhtml.g:652:2: ( rule__XmlElement__Alternatives_0 )
            {
             before(grammarAccess.getXmlElementAccess().getAlternatives_0()); 
            // InternalXhtml.g:653:2: ( rule__XmlElement__Alternatives_0 )
            // InternalXhtml.g:653:3: rule__XmlElement__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlElementAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__0__Impl"


    // $ANTLR start "rule__XmlElement__Group__1"
    // InternalXhtml.g:661:1: rule__XmlElement__Group__1 : rule__XmlElement__Group__1__Impl rule__XmlElement__Group__2 ;
    public final void rule__XmlElement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:665:1: ( rule__XmlElement__Group__1__Impl rule__XmlElement__Group__2 )
            // InternalXhtml.g:666:2: rule__XmlElement__Group__1__Impl rule__XmlElement__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__XmlElement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__1"


    // $ANTLR start "rule__XmlElement__Group__1__Impl"
    // InternalXhtml.g:673:1: rule__XmlElement__Group__1__Impl : ( ( rule__XmlElement__Group_1__0 )? ) ;
    public final void rule__XmlElement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:677:1: ( ( ( rule__XmlElement__Group_1__0 )? ) )
            // InternalXhtml.g:678:1: ( ( rule__XmlElement__Group_1__0 )? )
            {
            // InternalXhtml.g:678:1: ( ( rule__XmlElement__Group_1__0 )? )
            // InternalXhtml.g:679:2: ( rule__XmlElement__Group_1__0 )?
            {
             before(grammarAccess.getXmlElementAccess().getGroup_1()); 
            // InternalXhtml.g:680:2: ( rule__XmlElement__Group_1__0 )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==26) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalXhtml.g:680:3: rule__XmlElement__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__XmlElement__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getXmlElementAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__1__Impl"


    // $ANTLR start "rule__XmlElement__Group__2"
    // InternalXhtml.g:688:1: rule__XmlElement__Group__2 : rule__XmlElement__Group__2__Impl rule__XmlElement__Group__3 ;
    public final void rule__XmlElement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:692:1: ( rule__XmlElement__Group__2__Impl rule__XmlElement__Group__3 )
            // InternalXhtml.g:693:2: rule__XmlElement__Group__2__Impl rule__XmlElement__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__XmlElement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__2"


    // $ANTLR start "rule__XmlElement__Group__2__Impl"
    // InternalXhtml.g:700:1: rule__XmlElement__Group__2__Impl : ( ( rule__XmlElement__Group_2__0 )? ) ;
    public final void rule__XmlElement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:704:1: ( ( ( rule__XmlElement__Group_2__0 )? ) )
            // InternalXhtml.g:705:1: ( ( rule__XmlElement__Group_2__0 )? )
            {
            // InternalXhtml.g:705:1: ( ( rule__XmlElement__Group_2__0 )? )
            // InternalXhtml.g:706:2: ( rule__XmlElement__Group_2__0 )?
            {
             before(grammarAccess.getXmlElementAccess().getGroup_2()); 
            // InternalXhtml.g:707:2: ( rule__XmlElement__Group_2__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==27) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalXhtml.g:707:3: rule__XmlElement__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__XmlElement__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getXmlElementAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__2__Impl"


    // $ANTLR start "rule__XmlElement__Group__3"
    // InternalXhtml.g:715:1: rule__XmlElement__Group__3 : rule__XmlElement__Group__3__Impl rule__XmlElement__Group__4 ;
    public final void rule__XmlElement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:719:1: ( rule__XmlElement__Group__3__Impl rule__XmlElement__Group__4 )
            // InternalXhtml.g:720:2: rule__XmlElement__Group__3__Impl rule__XmlElement__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__XmlElement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__3"


    // $ANTLR start "rule__XmlElement__Group__3__Impl"
    // InternalXhtml.g:727:1: rule__XmlElement__Group__3__Impl : ( ( rule__XmlElement__AttributesAssignment_3 )* ) ;
    public final void rule__XmlElement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:731:1: ( ( ( rule__XmlElement__AttributesAssignment_3 )* ) )
            // InternalXhtml.g:732:1: ( ( rule__XmlElement__AttributesAssignment_3 )* )
            {
            // InternalXhtml.g:732:1: ( ( rule__XmlElement__AttributesAssignment_3 )* )
            // InternalXhtml.g:733:2: ( rule__XmlElement__AttributesAssignment_3 )*
            {
             before(grammarAccess.getXmlElementAccess().getAttributesAssignment_3()); 
            // InternalXhtml.g:734:2: ( rule__XmlElement__AttributesAssignment_3 )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==RULE_ID) ) {
                    int LA13_1 = input.LA(2);

                    if ( (LA13_1==30) ) {
                        alt13=1;
                    }


                }


                switch (alt13) {
            	case 1 :
            	    // InternalXhtml.g:734:3: rule__XmlElement__AttributesAssignment_3
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__XmlElement__AttributesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getXmlElementAccess().getAttributesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__3__Impl"


    // $ANTLR start "rule__XmlElement__Group__4"
    // InternalXhtml.g:742:1: rule__XmlElement__Group__4 : rule__XmlElement__Group__4__Impl rule__XmlElement__Group__5 ;
    public final void rule__XmlElement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:746:1: ( rule__XmlElement__Group__4__Impl rule__XmlElement__Group__5 )
            // InternalXhtml.g:747:2: rule__XmlElement__Group__4__Impl rule__XmlElement__Group__5
            {
            pushFollow(FOLLOW_5);
            rule__XmlElement__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__4"


    // $ANTLR start "rule__XmlElement__Group__4__Impl"
    // InternalXhtml.g:754:1: rule__XmlElement__Group__4__Impl : ( ( rule__XmlElement__StylesAssignment_4 )* ) ;
    public final void rule__XmlElement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:758:1: ( ( ( rule__XmlElement__StylesAssignment_4 )* ) )
            // InternalXhtml.g:759:1: ( ( rule__XmlElement__StylesAssignment_4 )* )
            {
            // InternalXhtml.g:759:1: ( ( rule__XmlElement__StylesAssignment_4 )* )
            // InternalXhtml.g:760:2: ( rule__XmlElement__StylesAssignment_4 )*
            {
             before(grammarAccess.getXmlElementAccess().getStylesAssignment_4()); 
            // InternalXhtml.g:761:2: ( rule__XmlElement__StylesAssignment_4 )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( ((LA14_0>=RULE_STD_TEXT && LA14_0<=RULE_ID)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalXhtml.g:761:3: rule__XmlElement__StylesAssignment_4
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__XmlElement__StylesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getXmlElementAccess().getStylesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__4__Impl"


    // $ANTLR start "rule__XmlElement__Group__5"
    // InternalXhtml.g:769:1: rule__XmlElement__Group__5 : rule__XmlElement__Group__5__Impl rule__XmlElement__Group__6 ;
    public final void rule__XmlElement__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:773:1: ( rule__XmlElement__Group__5__Impl rule__XmlElement__Group__6 )
            // InternalXhtml.g:774:2: rule__XmlElement__Group__5__Impl rule__XmlElement__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__XmlElement__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__5"


    // $ANTLR start "rule__XmlElement__Group__5__Impl"
    // InternalXhtml.g:781:1: rule__XmlElement__Group__5__Impl : ( '(' ) ;
    public final void rule__XmlElement__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:785:1: ( ( '(' ) )
            // InternalXhtml.g:786:1: ( '(' )
            {
            // InternalXhtml.g:786:1: ( '(' )
            // InternalXhtml.g:787:2: '('
            {
             before(grammarAccess.getXmlElementAccess().getLeftParenthesisKeyword_5()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getLeftParenthesisKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__5__Impl"


    // $ANTLR start "rule__XmlElement__Group__6"
    // InternalXhtml.g:796:1: rule__XmlElement__Group__6 : rule__XmlElement__Group__6__Impl rule__XmlElement__Group__7 ;
    public final void rule__XmlElement__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:800:1: ( rule__XmlElement__Group__6__Impl rule__XmlElement__Group__7 )
            // InternalXhtml.g:801:2: rule__XmlElement__Group__6__Impl rule__XmlElement__Group__7
            {
            pushFollow(FOLLOW_8);
            rule__XmlElement__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__6"


    // $ANTLR start "rule__XmlElement__Group__6__Impl"
    // InternalXhtml.g:808:1: rule__XmlElement__Group__6__Impl : ( ( rule__XmlElement__ContentsAssignment_6 )* ) ;
    public final void rule__XmlElement__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:812:1: ( ( ( rule__XmlElement__ContentsAssignment_6 )* ) )
            // InternalXhtml.g:813:1: ( ( rule__XmlElement__ContentsAssignment_6 )* )
            {
            // InternalXhtml.g:813:1: ( ( rule__XmlElement__ContentsAssignment_6 )* )
            // InternalXhtml.g:814:2: ( rule__XmlElement__ContentsAssignment_6 )*
            {
             before(grammarAccess.getXmlElementAccess().getContentsAssignment_6()); 
            // InternalXhtml.g:815:2: ( rule__XmlElement__ContentsAssignment_6 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( ((LA15_0>=RULE_STD_TEXT && LA15_0<=RULE_COMMENT_TEXT)||LA15_0==25||LA15_0==28) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalXhtml.g:815:3: rule__XmlElement__ContentsAssignment_6
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__XmlElement__ContentsAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getXmlElementAccess().getContentsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__6__Impl"


    // $ANTLR start "rule__XmlElement__Group__7"
    // InternalXhtml.g:823:1: rule__XmlElement__Group__7 : rule__XmlElement__Group__7__Impl ;
    public final void rule__XmlElement__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:827:1: ( rule__XmlElement__Group__7__Impl )
            // InternalXhtml.g:828:2: rule__XmlElement__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__7"


    // $ANTLR start "rule__XmlElement__Group__7__Impl"
    // InternalXhtml.g:834:1: rule__XmlElement__Group__7__Impl : ( ')' ) ;
    public final void rule__XmlElement__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:838:1: ( ( ')' ) )
            // InternalXhtml.g:839:1: ( ')' )
            {
            // InternalXhtml.g:839:1: ( ')' )
            // InternalXhtml.g:840:2: ')'
            {
             before(grammarAccess.getXmlElementAccess().getRightParenthesisKeyword_7()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getRightParenthesisKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group__7__Impl"


    // $ANTLR start "rule__XmlElement__Group_0_0__0"
    // InternalXhtml.g:850:1: rule__XmlElement__Group_0_0__0 : rule__XmlElement__Group_0_0__0__Impl rule__XmlElement__Group_0_0__1 ;
    public final void rule__XmlElement__Group_0_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:854:1: ( rule__XmlElement__Group_0_0__0__Impl rule__XmlElement__Group_0_0__1 )
            // InternalXhtml.g:855:2: rule__XmlElement__Group_0_0__0__Impl rule__XmlElement__Group_0_0__1
            {
            pushFollow(FOLLOW_9);
            rule__XmlElement__Group_0_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group_0_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_0_0__0"


    // $ANTLR start "rule__XmlElement__Group_0_0__0__Impl"
    // InternalXhtml.g:862:1: rule__XmlElement__Group_0_0__0__Impl : ( '/' ) ;
    public final void rule__XmlElement__Group_0_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:866:1: ( ( '/' ) )
            // InternalXhtml.g:867:1: ( '/' )
            {
            // InternalXhtml.g:867:1: ( '/' )
            // InternalXhtml.g:868:2: '/'
            {
             before(grammarAccess.getXmlElementAccess().getSolidusKeyword_0_0_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getSolidusKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_0_0__0__Impl"


    // $ANTLR start "rule__XmlElement__Group_0_0__1"
    // InternalXhtml.g:877:1: rule__XmlElement__Group_0_0__1 : rule__XmlElement__Group_0_0__1__Impl ;
    public final void rule__XmlElement__Group_0_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:881:1: ( rule__XmlElement__Group_0_0__1__Impl )
            // InternalXhtml.g:882:2: rule__XmlElement__Group_0_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Group_0_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_0_0__1"


    // $ANTLR start "rule__XmlElement__Group_0_0__1__Impl"
    // InternalXhtml.g:888:1: rule__XmlElement__Group_0_0__1__Impl : ( ( rule__XmlElement__SugarTypeAssignment_0_0_1 ) ) ;
    public final void rule__XmlElement__Group_0_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:892:1: ( ( ( rule__XmlElement__SugarTypeAssignment_0_0_1 ) ) )
            // InternalXhtml.g:893:1: ( ( rule__XmlElement__SugarTypeAssignment_0_0_1 ) )
            {
            // InternalXhtml.g:893:1: ( ( rule__XmlElement__SugarTypeAssignment_0_0_1 ) )
            // InternalXhtml.g:894:2: ( rule__XmlElement__SugarTypeAssignment_0_0_1 )
            {
             before(grammarAccess.getXmlElementAccess().getSugarTypeAssignment_0_0_1()); 
            // InternalXhtml.g:895:2: ( rule__XmlElement__SugarTypeAssignment_0_0_1 )
            // InternalXhtml.g:895:3: rule__XmlElement__SugarTypeAssignment_0_0_1
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__SugarTypeAssignment_0_0_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlElementAccess().getSugarTypeAssignment_0_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_0_0__1__Impl"


    // $ANTLR start "rule__XmlElement__Group_1__0"
    // InternalXhtml.g:904:1: rule__XmlElement__Group_1__0 : rule__XmlElement__Group_1__0__Impl rule__XmlElement__Group_1__1 ;
    public final void rule__XmlElement__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:908:1: ( rule__XmlElement__Group_1__0__Impl rule__XmlElement__Group_1__1 )
            // InternalXhtml.g:909:2: rule__XmlElement__Group_1__0__Impl rule__XmlElement__Group_1__1
            {
            pushFollow(FOLLOW_9);
            rule__XmlElement__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_1__0"


    // $ANTLR start "rule__XmlElement__Group_1__0__Impl"
    // InternalXhtml.g:916:1: rule__XmlElement__Group_1__0__Impl : ( '#' ) ;
    public final void rule__XmlElement__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:920:1: ( ( '#' ) )
            // InternalXhtml.g:921:1: ( '#' )
            {
            // InternalXhtml.g:921:1: ( '#' )
            // InternalXhtml.g:922:2: '#'
            {
             before(grammarAccess.getXmlElementAccess().getNumberSignKeyword_1_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getNumberSignKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_1__0__Impl"


    // $ANTLR start "rule__XmlElement__Group_1__1"
    // InternalXhtml.g:931:1: rule__XmlElement__Group_1__1 : rule__XmlElement__Group_1__1__Impl ;
    public final void rule__XmlElement__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:935:1: ( rule__XmlElement__Group_1__1__Impl )
            // InternalXhtml.g:936:2: rule__XmlElement__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_1__1"


    // $ANTLR start "rule__XmlElement__Group_1__1__Impl"
    // InternalXhtml.g:942:1: rule__XmlElement__Group_1__1__Impl : ( ( rule__XmlElement__IdAssignment_1_1 ) ) ;
    public final void rule__XmlElement__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:946:1: ( ( ( rule__XmlElement__IdAssignment_1_1 ) ) )
            // InternalXhtml.g:947:1: ( ( rule__XmlElement__IdAssignment_1_1 ) )
            {
            // InternalXhtml.g:947:1: ( ( rule__XmlElement__IdAssignment_1_1 ) )
            // InternalXhtml.g:948:2: ( rule__XmlElement__IdAssignment_1_1 )
            {
             before(grammarAccess.getXmlElementAccess().getIdAssignment_1_1()); 
            // InternalXhtml.g:949:2: ( rule__XmlElement__IdAssignment_1_1 )
            // InternalXhtml.g:949:3: rule__XmlElement__IdAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__IdAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlElementAccess().getIdAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_1__1__Impl"


    // $ANTLR start "rule__XmlElement__Group_2__0"
    // InternalXhtml.g:958:1: rule__XmlElement__Group_2__0 : rule__XmlElement__Group_2__0__Impl rule__XmlElement__Group_2__1 ;
    public final void rule__XmlElement__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:962:1: ( rule__XmlElement__Group_2__0__Impl rule__XmlElement__Group_2__1 )
            // InternalXhtml.g:963:2: rule__XmlElement__Group_2__0__Impl rule__XmlElement__Group_2__1
            {
            pushFollow(FOLLOW_9);
            rule__XmlElement__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlElement__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_2__0"


    // $ANTLR start "rule__XmlElement__Group_2__0__Impl"
    // InternalXhtml.g:970:1: rule__XmlElement__Group_2__0__Impl : ( '.' ) ;
    public final void rule__XmlElement__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:974:1: ( ( '.' ) )
            // InternalXhtml.g:975:1: ( '.' )
            {
            // InternalXhtml.g:975:1: ( '.' )
            // InternalXhtml.g:976:2: '.'
            {
             before(grammarAccess.getXmlElementAccess().getFullStopKeyword_2_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getFullStopKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_2__0__Impl"


    // $ANTLR start "rule__XmlElement__Group_2__1"
    // InternalXhtml.g:985:1: rule__XmlElement__Group_2__1 : rule__XmlElement__Group_2__1__Impl ;
    public final void rule__XmlElement__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:989:1: ( rule__XmlElement__Group_2__1__Impl )
            // InternalXhtml.g:990:2: rule__XmlElement__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_2__1"


    // $ANTLR start "rule__XmlElement__Group_2__1__Impl"
    // InternalXhtml.g:996:1: rule__XmlElement__Group_2__1__Impl : ( ( rule__XmlElement__ClassNameAssignment_2_1 ) ) ;
    public final void rule__XmlElement__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1000:1: ( ( ( rule__XmlElement__ClassNameAssignment_2_1 ) ) )
            // InternalXhtml.g:1001:1: ( ( rule__XmlElement__ClassNameAssignment_2_1 ) )
            {
            // InternalXhtml.g:1001:1: ( ( rule__XmlElement__ClassNameAssignment_2_1 ) )
            // InternalXhtml.g:1002:2: ( rule__XmlElement__ClassNameAssignment_2_1 )
            {
             before(grammarAccess.getXmlElementAccess().getClassNameAssignment_2_1()); 
            // InternalXhtml.g:1003:2: ( rule__XmlElement__ClassNameAssignment_2_1 )
            // InternalXhtml.g:1003:3: rule__XmlElement__ClassNameAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__XmlElement__ClassNameAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getXmlElementAccess().getClassNameAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__Group_2__1__Impl"


    // $ANTLR start "rule__InnerText__Group__0"
    // InternalXhtml.g:1012:1: rule__InnerText__Group__0 : rule__InnerText__Group__0__Impl rule__InnerText__Group__1 ;
    public final void rule__InnerText__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1016:1: ( rule__InnerText__Group__0__Impl rule__InnerText__Group__1 )
            // InternalXhtml.g:1017:2: rule__InnerText__Group__0__Impl rule__InnerText__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__InnerText__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InnerText__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InnerText__Group__0"


    // $ANTLR start "rule__InnerText__Group__0__Impl"
    // InternalXhtml.g:1024:1: rule__InnerText__Group__0__Impl : ( () ) ;
    public final void rule__InnerText__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1028:1: ( ( () ) )
            // InternalXhtml.g:1029:1: ( () )
            {
            // InternalXhtml.g:1029:1: ( () )
            // InternalXhtml.g:1030:2: ()
            {
             before(grammarAccess.getInnerTextAccess().getInnerTextAction_0()); 
            // InternalXhtml.g:1031:2: ()
            // InternalXhtml.g:1031:3: 
            {
            }

             after(grammarAccess.getInnerTextAccess().getInnerTextAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InnerText__Group__0__Impl"


    // $ANTLR start "rule__InnerText__Group__1"
    // InternalXhtml.g:1039:1: rule__InnerText__Group__1 : rule__InnerText__Group__1__Impl ;
    public final void rule__InnerText__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1043:1: ( rule__InnerText__Group__1__Impl )
            // InternalXhtml.g:1044:2: rule__InnerText__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InnerText__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InnerText__Group__1"


    // $ANTLR start "rule__InnerText__Group__1__Impl"
    // InternalXhtml.g:1050:1: rule__InnerText__Group__1__Impl : ( ( rule__InnerText__TextAssignment_1 ) ) ;
    public final void rule__InnerText__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1054:1: ( ( ( rule__InnerText__TextAssignment_1 ) ) )
            // InternalXhtml.g:1055:1: ( ( rule__InnerText__TextAssignment_1 ) )
            {
            // InternalXhtml.g:1055:1: ( ( rule__InnerText__TextAssignment_1 ) )
            // InternalXhtml.g:1056:2: ( rule__InnerText__TextAssignment_1 )
            {
             before(grammarAccess.getInnerTextAccess().getTextAssignment_1()); 
            // InternalXhtml.g:1057:2: ( rule__InnerText__TextAssignment_1 )
            // InternalXhtml.g:1057:3: rule__InnerText__TextAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__InnerText__TextAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInnerTextAccess().getTextAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InnerText__Group__1__Impl"


    // $ANTLR start "rule__Variable__Group__0"
    // InternalXhtml.g:1066:1: rule__Variable__Group__0 : rule__Variable__Group__0__Impl rule__Variable__Group__1 ;
    public final void rule__Variable__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1070:1: ( rule__Variable__Group__0__Impl rule__Variable__Group__1 )
            // InternalXhtml.g:1071:2: rule__Variable__Group__0__Impl rule__Variable__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Variable__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__0"


    // $ANTLR start "rule__Variable__Group__0__Impl"
    // InternalXhtml.g:1078:1: rule__Variable__Group__0__Impl : ( '$' ) ;
    public final void rule__Variable__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1082:1: ( ( '$' ) )
            // InternalXhtml.g:1083:1: ( '$' )
            {
            // InternalXhtml.g:1083:1: ( '$' )
            // InternalXhtml.g:1084:2: '$'
            {
             before(grammarAccess.getVariableAccess().getDollarSignKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getDollarSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__0__Impl"


    // $ANTLR start "rule__Variable__Group__1"
    // InternalXhtml.g:1093:1: rule__Variable__Group__1 : rule__Variable__Group__1__Impl rule__Variable__Group__2 ;
    public final void rule__Variable__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1097:1: ( rule__Variable__Group__1__Impl rule__Variable__Group__2 )
            // InternalXhtml.g:1098:2: rule__Variable__Group__1__Impl rule__Variable__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__Variable__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__1"


    // $ANTLR start "rule__Variable__Group__1__Impl"
    // InternalXhtml.g:1105:1: rule__Variable__Group__1__Impl : ( ( rule__Variable__NameAssignment_1 ) ) ;
    public final void rule__Variable__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1109:1: ( ( ( rule__Variable__NameAssignment_1 ) ) )
            // InternalXhtml.g:1110:1: ( ( rule__Variable__NameAssignment_1 ) )
            {
            // InternalXhtml.g:1110:1: ( ( rule__Variable__NameAssignment_1 ) )
            // InternalXhtml.g:1111:2: ( rule__Variable__NameAssignment_1 )
            {
             before(grammarAccess.getVariableAccess().getNameAssignment_1()); 
            // InternalXhtml.g:1112:2: ( rule__Variable__NameAssignment_1 )
            // InternalXhtml.g:1112:3: rule__Variable__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Variable__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__1__Impl"


    // $ANTLR start "rule__Variable__Group__2"
    // InternalXhtml.g:1120:1: rule__Variable__Group__2 : rule__Variable__Group__2__Impl rule__Variable__Group__3 ;
    public final void rule__Variable__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1124:1: ( rule__Variable__Group__2__Impl rule__Variable__Group__3 )
            // InternalXhtml.g:1125:2: rule__Variable__Group__2__Impl rule__Variable__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__Variable__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__2"


    // $ANTLR start "rule__Variable__Group__2__Impl"
    // InternalXhtml.g:1132:1: rule__Variable__Group__2__Impl : ( ( rule__Variable__Group_2__0 )? ) ;
    public final void rule__Variable__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1136:1: ( ( ( rule__Variable__Group_2__0 )? ) )
            // InternalXhtml.g:1137:1: ( ( rule__Variable__Group_2__0 )? )
            {
            // InternalXhtml.g:1137:1: ( ( rule__Variable__Group_2__0 )? )
            // InternalXhtml.g:1138:2: ( rule__Variable__Group_2__0 )?
            {
             before(grammarAccess.getVariableAccess().getGroup_2()); 
            // InternalXhtml.g:1139:2: ( rule__Variable__Group_2__0 )?
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==30) ) {
                alt16=1;
            }
            switch (alt16) {
                case 1 :
                    // InternalXhtml.g:1139:3: rule__Variable__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Variable__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVariableAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__2__Impl"


    // $ANTLR start "rule__Variable__Group__3"
    // InternalXhtml.g:1147:1: rule__Variable__Group__3 : rule__Variable__Group__3__Impl ;
    public final void rule__Variable__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1151:1: ( rule__Variable__Group__3__Impl )
            // InternalXhtml.g:1152:2: rule__Variable__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__3"


    // $ANTLR start "rule__Variable__Group__3__Impl"
    // InternalXhtml.g:1158:1: rule__Variable__Group__3__Impl : ( ';' ) ;
    public final void rule__Variable__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1162:1: ( ( ';' ) )
            // InternalXhtml.g:1163:1: ( ';' )
            {
            // InternalXhtml.g:1163:1: ( ';' )
            // InternalXhtml.g:1164:2: ';'
            {
             before(grammarAccess.getVariableAccess().getSemicolonKeyword_3()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getSemicolonKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group__3__Impl"


    // $ANTLR start "rule__Variable__Group_2__0"
    // InternalXhtml.g:1174:1: rule__Variable__Group_2__0 : rule__Variable__Group_2__0__Impl rule__Variable__Group_2__1 ;
    public final void rule__Variable__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1178:1: ( rule__Variable__Group_2__0__Impl rule__Variable__Group_2__1 )
            // InternalXhtml.g:1179:2: rule__Variable__Group_2__0__Impl rule__Variable__Group_2__1
            {
            pushFollow(FOLLOW_12);
            rule__Variable__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Variable__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group_2__0"


    // $ANTLR start "rule__Variable__Group_2__0__Impl"
    // InternalXhtml.g:1186:1: rule__Variable__Group_2__0__Impl : ( '=' ) ;
    public final void rule__Variable__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1190:1: ( ( '=' ) )
            // InternalXhtml.g:1191:1: ( '=' )
            {
            // InternalXhtml.g:1191:1: ( '=' )
            // InternalXhtml.g:1192:2: '='
            {
             before(grammarAccess.getVariableAccess().getEqualsSignKeyword_2_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getEqualsSignKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group_2__0__Impl"


    // $ANTLR start "rule__Variable__Group_2__1"
    // InternalXhtml.g:1201:1: rule__Variable__Group_2__1 : rule__Variable__Group_2__1__Impl ;
    public final void rule__Variable__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1205:1: ( rule__Variable__Group_2__1__Impl )
            // InternalXhtml.g:1206:2: rule__Variable__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Variable__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group_2__1"


    // $ANTLR start "rule__Variable__Group_2__1__Impl"
    // InternalXhtml.g:1212:1: rule__Variable__Group_2__1__Impl : ( ( rule__Variable__ValueAssignment_2_1 ) ) ;
    public final void rule__Variable__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1216:1: ( ( ( rule__Variable__ValueAssignment_2_1 ) ) )
            // InternalXhtml.g:1217:1: ( ( rule__Variable__ValueAssignment_2_1 ) )
            {
            // InternalXhtml.g:1217:1: ( ( rule__Variable__ValueAssignment_2_1 ) )
            // InternalXhtml.g:1218:2: ( rule__Variable__ValueAssignment_2_1 )
            {
             before(grammarAccess.getVariableAccess().getValueAssignment_2_1()); 
            // InternalXhtml.g:1219:2: ( rule__Variable__ValueAssignment_2_1 )
            // InternalXhtml.g:1219:3: rule__Variable__ValueAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Variable__ValueAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getValueAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__Group_2__1__Impl"


    // $ANTLR start "rule__XmlAttribute__Group__0"
    // InternalXhtml.g:1228:1: rule__XmlAttribute__Group__0 : rule__XmlAttribute__Group__0__Impl rule__XmlAttribute__Group__1 ;
    public final void rule__XmlAttribute__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1232:1: ( rule__XmlAttribute__Group__0__Impl rule__XmlAttribute__Group__1 )
            // InternalXhtml.g:1233:2: rule__XmlAttribute__Group__0__Impl rule__XmlAttribute__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__XmlAttribute__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__0"


    // $ANTLR start "rule__XmlAttribute__Group__0__Impl"
    // InternalXhtml.g:1240:1: rule__XmlAttribute__Group__0__Impl : ( ( rule__XmlAttribute__NameAssignment_0 ) ) ;
    public final void rule__XmlAttribute__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1244:1: ( ( ( rule__XmlAttribute__NameAssignment_0 ) ) )
            // InternalXhtml.g:1245:1: ( ( rule__XmlAttribute__NameAssignment_0 ) )
            {
            // InternalXhtml.g:1245:1: ( ( rule__XmlAttribute__NameAssignment_0 ) )
            // InternalXhtml.g:1246:2: ( rule__XmlAttribute__NameAssignment_0 )
            {
             before(grammarAccess.getXmlAttributeAccess().getNameAssignment_0()); 
            // InternalXhtml.g:1247:2: ( rule__XmlAttribute__NameAssignment_0 )
            // InternalXhtml.g:1247:3: rule__XmlAttribute__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__0__Impl"


    // $ANTLR start "rule__XmlAttribute__Group__1"
    // InternalXhtml.g:1255:1: rule__XmlAttribute__Group__1 : rule__XmlAttribute__Group__1__Impl rule__XmlAttribute__Group__2 ;
    public final void rule__XmlAttribute__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1259:1: ( rule__XmlAttribute__Group__1__Impl rule__XmlAttribute__Group__2 )
            // InternalXhtml.g:1260:2: rule__XmlAttribute__Group__1__Impl rule__XmlAttribute__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__XmlAttribute__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__1"


    // $ANTLR start "rule__XmlAttribute__Group__1__Impl"
    // InternalXhtml.g:1267:1: rule__XmlAttribute__Group__1__Impl : ( '=' ) ;
    public final void rule__XmlAttribute__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1271:1: ( ( '=' ) )
            // InternalXhtml.g:1272:1: ( '=' )
            {
            // InternalXhtml.g:1272:1: ( '=' )
            // InternalXhtml.g:1273:2: '='
            {
             before(grammarAccess.getXmlAttributeAccess().getEqualsSignKeyword_1()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getXmlAttributeAccess().getEqualsSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__1__Impl"


    // $ANTLR start "rule__XmlAttribute__Group__2"
    // InternalXhtml.g:1282:1: rule__XmlAttribute__Group__2 : rule__XmlAttribute__Group__2__Impl ;
    public final void rule__XmlAttribute__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1286:1: ( rule__XmlAttribute__Group__2__Impl )
            // InternalXhtml.g:1287:2: rule__XmlAttribute__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__2"


    // $ANTLR start "rule__XmlAttribute__Group__2__Impl"
    // InternalXhtml.g:1293:1: rule__XmlAttribute__Group__2__Impl : ( ( rule__XmlAttribute__ValueAssignment_2 ) ) ;
    public final void rule__XmlAttribute__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1297:1: ( ( ( rule__XmlAttribute__ValueAssignment_2 ) ) )
            // InternalXhtml.g:1298:1: ( ( rule__XmlAttribute__ValueAssignment_2 ) )
            {
            // InternalXhtml.g:1298:1: ( ( rule__XmlAttribute__ValueAssignment_2 ) )
            // InternalXhtml.g:1299:2: ( rule__XmlAttribute__ValueAssignment_2 )
            {
             before(grammarAccess.getXmlAttributeAccess().getValueAssignment_2()); 
            // InternalXhtml.g:1300:2: ( rule__XmlAttribute__ValueAssignment_2 )
            // InternalXhtml.g:1300:3: rule__XmlAttribute__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__Group__2__Impl"


    // $ANTLR start "rule__XmlStyle__Group__0"
    // InternalXhtml.g:1309:1: rule__XmlStyle__Group__0 : rule__XmlStyle__Group__0__Impl rule__XmlStyle__Group__1 ;
    public final void rule__XmlStyle__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1313:1: ( rule__XmlStyle__Group__0__Impl rule__XmlStyle__Group__1 )
            // InternalXhtml.g:1314:2: rule__XmlStyle__Group__0__Impl rule__XmlStyle__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__XmlStyle__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlStyle__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__Group__0"


    // $ANTLR start "rule__XmlStyle__Group__0__Impl"
    // InternalXhtml.g:1321:1: rule__XmlStyle__Group__0__Impl : ( ( rule__XmlStyle__AttributeAssignment_0 ) ) ;
    public final void rule__XmlStyle__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1325:1: ( ( ( rule__XmlStyle__AttributeAssignment_0 ) ) )
            // InternalXhtml.g:1326:1: ( ( rule__XmlStyle__AttributeAssignment_0 ) )
            {
            // InternalXhtml.g:1326:1: ( ( rule__XmlStyle__AttributeAssignment_0 ) )
            // InternalXhtml.g:1327:2: ( rule__XmlStyle__AttributeAssignment_0 )
            {
             before(grammarAccess.getXmlStyleAccess().getAttributeAssignment_0()); 
            // InternalXhtml.g:1328:2: ( rule__XmlStyle__AttributeAssignment_0 )
            // InternalXhtml.g:1328:3: rule__XmlStyle__AttributeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlStyle__AttributeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlStyleAccess().getAttributeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__Group__0__Impl"


    // $ANTLR start "rule__XmlStyle__Group__1"
    // InternalXhtml.g:1336:1: rule__XmlStyle__Group__1 : rule__XmlStyle__Group__1__Impl rule__XmlStyle__Group__2 ;
    public final void rule__XmlStyle__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1340:1: ( rule__XmlStyle__Group__1__Impl rule__XmlStyle__Group__2 )
            // InternalXhtml.g:1341:2: rule__XmlStyle__Group__1__Impl rule__XmlStyle__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__XmlStyle__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlStyle__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__Group__1"


    // $ANTLR start "rule__XmlStyle__Group__1__Impl"
    // InternalXhtml.g:1348:1: rule__XmlStyle__Group__1__Impl : ( ':' ) ;
    public final void rule__XmlStyle__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1352:1: ( ( ':' ) )
            // InternalXhtml.g:1353:1: ( ':' )
            {
            // InternalXhtml.g:1353:1: ( ':' )
            // InternalXhtml.g:1354:2: ':'
            {
             before(grammarAccess.getXmlStyleAccess().getColonKeyword_1()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getXmlStyleAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__Group__1__Impl"


    // $ANTLR start "rule__XmlStyle__Group__2"
    // InternalXhtml.g:1363:1: rule__XmlStyle__Group__2 : rule__XmlStyle__Group__2__Impl ;
    public final void rule__XmlStyle__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1367:1: ( rule__XmlStyle__Group__2__Impl )
            // InternalXhtml.g:1368:2: rule__XmlStyle__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlStyle__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__Group__2"


    // $ANTLR start "rule__XmlStyle__Group__2__Impl"
    // InternalXhtml.g:1374:1: rule__XmlStyle__Group__2__Impl : ( ( rule__XmlStyle__ValueAssignment_2 ) ) ;
    public final void rule__XmlStyle__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1378:1: ( ( ( rule__XmlStyle__ValueAssignment_2 ) ) )
            // InternalXhtml.g:1379:1: ( ( rule__XmlStyle__ValueAssignment_2 ) )
            {
            // InternalXhtml.g:1379:1: ( ( rule__XmlStyle__ValueAssignment_2 ) )
            // InternalXhtml.g:1380:2: ( rule__XmlStyle__ValueAssignment_2 )
            {
             before(grammarAccess.getXmlStyleAccess().getValueAssignment_2()); 
            // InternalXhtml.g:1381:2: ( rule__XmlStyle__ValueAssignment_2 )
            // InternalXhtml.g:1381:3: rule__XmlStyle__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__XmlStyle__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXmlStyleAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__Group__2__Impl"


    // $ANTLR start "rule__XmlInt__Group__0"
    // InternalXhtml.g:1390:1: rule__XmlInt__Group__0 : rule__XmlInt__Group__0__Impl rule__XmlInt__Group__1 ;
    public final void rule__XmlInt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1394:1: ( rule__XmlInt__Group__0__Impl rule__XmlInt__Group__1 )
            // InternalXhtml.g:1395:2: rule__XmlInt__Group__0__Impl rule__XmlInt__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__XmlInt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XmlInt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlInt__Group__0"


    // $ANTLR start "rule__XmlInt__Group__0__Impl"
    // InternalXhtml.g:1402:1: rule__XmlInt__Group__0__Impl : ( ( rule__XmlInt__Alternatives_0 ) ) ;
    public final void rule__XmlInt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1406:1: ( ( ( rule__XmlInt__Alternatives_0 ) ) )
            // InternalXhtml.g:1407:1: ( ( rule__XmlInt__Alternatives_0 ) )
            {
            // InternalXhtml.g:1407:1: ( ( rule__XmlInt__Alternatives_0 ) )
            // InternalXhtml.g:1408:2: ( rule__XmlInt__Alternatives_0 )
            {
             before(grammarAccess.getXmlIntAccess().getAlternatives_0()); 
            // InternalXhtml.g:1409:2: ( rule__XmlInt__Alternatives_0 )
            // InternalXhtml.g:1409:3: rule__XmlInt__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlInt__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlIntAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlInt__Group__0__Impl"


    // $ANTLR start "rule__XmlInt__Group__1"
    // InternalXhtml.g:1417:1: rule__XmlInt__Group__1 : rule__XmlInt__Group__1__Impl ;
    public final void rule__XmlInt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1421:1: ( rule__XmlInt__Group__1__Impl )
            // InternalXhtml.g:1422:2: rule__XmlInt__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XmlInt__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlInt__Group__1"


    // $ANTLR start "rule__XmlInt__Group__1__Impl"
    // InternalXhtml.g:1428:1: rule__XmlInt__Group__1__Impl : ( ( rule__XmlInt__Alternatives_1 )* ) ;
    public final void rule__XmlInt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1432:1: ( ( ( rule__XmlInt__Alternatives_1 )* ) )
            // InternalXhtml.g:1433:1: ( ( rule__XmlInt__Alternatives_1 )* )
            {
            // InternalXhtml.g:1433:1: ( ( rule__XmlInt__Alternatives_1 )* )
            // InternalXhtml.g:1434:2: ( rule__XmlInt__Alternatives_1 )*
            {
             before(grammarAccess.getXmlIntAccess().getAlternatives_1()); 
            // InternalXhtml.g:1435:2: ( rule__XmlInt__Alternatives_1 )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( ((LA17_0>=13 && LA17_0<=22)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalXhtml.g:1435:3: rule__XmlInt__Alternatives_1
            	    {
            	    pushFollow(FOLLOW_16);
            	    rule__XmlInt__Alternatives_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

             after(grammarAccess.getXmlIntAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlInt__Group__1__Impl"


    // $ANTLR start "rule__Domainmodel__ElementsAssignment"
    // InternalXhtml.g:1444:1: rule__Domainmodel__ElementsAssignment : ( ruleType ) ;
    public final void rule__Domainmodel__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1448:1: ( ( ruleType ) )
            // InternalXhtml.g:1449:2: ( ruleType )
            {
            // InternalXhtml.g:1449:2: ( ruleType )
            // InternalXhtml.g:1450:3: ruleType
            {
             before(grammarAccess.getDomainmodelAccess().getElementsTypeParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getElementsTypeParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__ElementsAssignment"


    // $ANTLR start "rule__Comment__TextAssignment_1"
    // InternalXhtml.g:1459:1: rule__Comment__TextAssignment_1 : ( RULE_COMMENT_TEXT ) ;
    public final void rule__Comment__TextAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1463:1: ( ( RULE_COMMENT_TEXT ) )
            // InternalXhtml.g:1464:2: ( RULE_COMMENT_TEXT )
            {
            // InternalXhtml.g:1464:2: ( RULE_COMMENT_TEXT )
            // InternalXhtml.g:1465:3: RULE_COMMENT_TEXT
            {
             before(grammarAccess.getCommentAccess().getTextCOMMENT_TEXTTerminalRuleCall_1_0()); 
            match(input,RULE_COMMENT_TEXT,FOLLOW_2); 
             after(grammarAccess.getCommentAccess().getTextCOMMENT_TEXTTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Comment__TextAssignment_1"


    // $ANTLR start "rule__XmlElement__SugarTypeAssignment_0_0_1"
    // InternalXhtml.g:1474:1: rule__XmlElement__SugarTypeAssignment_0_0_1 : ( RULE_ID ) ;
    public final void rule__XmlElement__SugarTypeAssignment_0_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1478:1: ( ( RULE_ID ) )
            // InternalXhtml.g:1479:2: ( RULE_ID )
            {
            // InternalXhtml.g:1479:2: ( RULE_ID )
            // InternalXhtml.g:1480:3: RULE_ID
            {
             before(grammarAccess.getXmlElementAccess().getSugarTypeIDTerminalRuleCall_0_0_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getSugarTypeIDTerminalRuleCall_0_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__SugarTypeAssignment_0_0_1"


    // $ANTLR start "rule__XmlElement__TypeAssignment_0_1"
    // InternalXhtml.g:1489:1: rule__XmlElement__TypeAssignment_0_1 : ( RULE_ID ) ;
    public final void rule__XmlElement__TypeAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1493:1: ( ( RULE_ID ) )
            // InternalXhtml.g:1494:2: ( RULE_ID )
            {
            // InternalXhtml.g:1494:2: ( RULE_ID )
            // InternalXhtml.g:1495:3: RULE_ID
            {
             before(grammarAccess.getXmlElementAccess().getTypeIDTerminalRuleCall_0_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getTypeIDTerminalRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__TypeAssignment_0_1"


    // $ANTLR start "rule__XmlElement__IdAssignment_1_1"
    // InternalXhtml.g:1504:1: rule__XmlElement__IdAssignment_1_1 : ( RULE_ID ) ;
    public final void rule__XmlElement__IdAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1508:1: ( ( RULE_ID ) )
            // InternalXhtml.g:1509:2: ( RULE_ID )
            {
            // InternalXhtml.g:1509:2: ( RULE_ID )
            // InternalXhtml.g:1510:3: RULE_ID
            {
             before(grammarAccess.getXmlElementAccess().getIdIDTerminalRuleCall_1_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getIdIDTerminalRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__IdAssignment_1_1"


    // $ANTLR start "rule__XmlElement__ClassNameAssignment_2_1"
    // InternalXhtml.g:1519:1: rule__XmlElement__ClassNameAssignment_2_1 : ( RULE_ID ) ;
    public final void rule__XmlElement__ClassNameAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1523:1: ( ( RULE_ID ) )
            // InternalXhtml.g:1524:2: ( RULE_ID )
            {
            // InternalXhtml.g:1524:2: ( RULE_ID )
            // InternalXhtml.g:1525:3: RULE_ID
            {
             before(grammarAccess.getXmlElementAccess().getClassNameIDTerminalRuleCall_2_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlElementAccess().getClassNameIDTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__ClassNameAssignment_2_1"


    // $ANTLR start "rule__XmlElement__AttributesAssignment_3"
    // InternalXhtml.g:1534:1: rule__XmlElement__AttributesAssignment_3 : ( ruleXmlAttribute ) ;
    public final void rule__XmlElement__AttributesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1538:1: ( ( ruleXmlAttribute ) )
            // InternalXhtml.g:1539:2: ( ruleXmlAttribute )
            {
            // InternalXhtml.g:1539:2: ( ruleXmlAttribute )
            // InternalXhtml.g:1540:3: ruleXmlAttribute
            {
             before(grammarAccess.getXmlElementAccess().getAttributesXmlAttributeParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlAttribute();

            state._fsp--;

             after(grammarAccess.getXmlElementAccess().getAttributesXmlAttributeParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__AttributesAssignment_3"


    // $ANTLR start "rule__XmlElement__StylesAssignment_4"
    // InternalXhtml.g:1549:1: rule__XmlElement__StylesAssignment_4 : ( ruleXmlStyle ) ;
    public final void rule__XmlElement__StylesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1553:1: ( ( ruleXmlStyle ) )
            // InternalXhtml.g:1554:2: ( ruleXmlStyle )
            {
            // InternalXhtml.g:1554:2: ( ruleXmlStyle )
            // InternalXhtml.g:1555:3: ruleXmlStyle
            {
             before(grammarAccess.getXmlElementAccess().getStylesXmlStyleParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleXmlStyle();

            state._fsp--;

             after(grammarAccess.getXmlElementAccess().getStylesXmlStyleParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__StylesAssignment_4"


    // $ANTLR start "rule__XmlElement__ContentsAssignment_6"
    // InternalXhtml.g:1564:1: rule__XmlElement__ContentsAssignment_6 : ( ruleType ) ;
    public final void rule__XmlElement__ContentsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1568:1: ( ( ruleType ) )
            // InternalXhtml.g:1569:2: ( ruleType )
            {
            // InternalXhtml.g:1569:2: ( ruleType )
            // InternalXhtml.g:1570:3: ruleType
            {
             before(grammarAccess.getXmlElementAccess().getContentsTypeParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getXmlElementAccess().getContentsTypeParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlElement__ContentsAssignment_6"


    // $ANTLR start "rule__InnerText__TextAssignment_1"
    // InternalXhtml.g:1579:1: rule__InnerText__TextAssignment_1 : ( ( rule__InnerText__TextAlternatives_1_0 ) ) ;
    public final void rule__InnerText__TextAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1583:1: ( ( ( rule__InnerText__TextAlternatives_1_0 ) ) )
            // InternalXhtml.g:1584:2: ( ( rule__InnerText__TextAlternatives_1_0 ) )
            {
            // InternalXhtml.g:1584:2: ( ( rule__InnerText__TextAlternatives_1_0 ) )
            // InternalXhtml.g:1585:3: ( rule__InnerText__TextAlternatives_1_0 )
            {
             before(grammarAccess.getInnerTextAccess().getTextAlternatives_1_0()); 
            // InternalXhtml.g:1586:3: ( rule__InnerText__TextAlternatives_1_0 )
            // InternalXhtml.g:1586:4: rule__InnerText__TextAlternatives_1_0
            {
            pushFollow(FOLLOW_2);
            rule__InnerText__TextAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getInnerTextAccess().getTextAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InnerText__TextAssignment_1"


    // $ANTLR start "rule__Variable__NameAssignment_1"
    // InternalXhtml.g:1594:1: rule__Variable__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Variable__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1598:1: ( ( RULE_ID ) )
            // InternalXhtml.g:1599:2: ( RULE_ID )
            {
            // InternalXhtml.g:1599:2: ( RULE_ID )
            // InternalXhtml.g:1600:3: RULE_ID
            {
             before(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__NameAssignment_1"


    // $ANTLR start "rule__Variable__ValueAssignment_2_1"
    // InternalXhtml.g:1609:1: rule__Variable__ValueAssignment_2_1 : ( ( rule__Variable__ValueAlternatives_2_1_0 ) ) ;
    public final void rule__Variable__ValueAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1613:1: ( ( ( rule__Variable__ValueAlternatives_2_1_0 ) ) )
            // InternalXhtml.g:1614:2: ( ( rule__Variable__ValueAlternatives_2_1_0 ) )
            {
            // InternalXhtml.g:1614:2: ( ( rule__Variable__ValueAlternatives_2_1_0 ) )
            // InternalXhtml.g:1615:3: ( rule__Variable__ValueAlternatives_2_1_0 )
            {
             before(grammarAccess.getVariableAccess().getValueAlternatives_2_1_0()); 
            // InternalXhtml.g:1616:3: ( rule__Variable__ValueAlternatives_2_1_0 )
            // InternalXhtml.g:1616:4: rule__Variable__ValueAlternatives_2_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Variable__ValueAlternatives_2_1_0();

            state._fsp--;


            }

             after(grammarAccess.getVariableAccess().getValueAlternatives_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Variable__ValueAssignment_2_1"


    // $ANTLR start "rule__XmlAttribute__NameAssignment_0"
    // InternalXhtml.g:1624:1: rule__XmlAttribute__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__XmlAttribute__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1628:1: ( ( RULE_ID ) )
            // InternalXhtml.g:1629:2: ( RULE_ID )
            {
            // InternalXhtml.g:1629:2: ( RULE_ID )
            // InternalXhtml.g:1630:3: RULE_ID
            {
             before(grammarAccess.getXmlAttributeAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getXmlAttributeAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__NameAssignment_0"


    // $ANTLR start "rule__XmlAttribute__ValueAssignment_2"
    // InternalXhtml.g:1639:1: rule__XmlAttribute__ValueAssignment_2 : ( ( rule__XmlAttribute__ValueAlternatives_2_0 ) ) ;
    public final void rule__XmlAttribute__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1643:1: ( ( ( rule__XmlAttribute__ValueAlternatives_2_0 ) ) )
            // InternalXhtml.g:1644:2: ( ( rule__XmlAttribute__ValueAlternatives_2_0 ) )
            {
            // InternalXhtml.g:1644:2: ( ( rule__XmlAttribute__ValueAlternatives_2_0 ) )
            // InternalXhtml.g:1645:3: ( rule__XmlAttribute__ValueAlternatives_2_0 )
            {
             before(grammarAccess.getXmlAttributeAccess().getValueAlternatives_2_0()); 
            // InternalXhtml.g:1646:3: ( rule__XmlAttribute__ValueAlternatives_2_0 )
            // InternalXhtml.g:1646:4: rule__XmlAttribute__ValueAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlAttribute__ValueAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlAttributeAccess().getValueAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlAttribute__ValueAssignment_2"


    // $ANTLR start "rule__XmlStyle__AttributeAssignment_0"
    // InternalXhtml.g:1654:1: rule__XmlStyle__AttributeAssignment_0 : ( ( rule__XmlStyle__AttributeAlternatives_0_0 ) ) ;
    public final void rule__XmlStyle__AttributeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1658:1: ( ( ( rule__XmlStyle__AttributeAlternatives_0_0 ) ) )
            // InternalXhtml.g:1659:2: ( ( rule__XmlStyle__AttributeAlternatives_0_0 ) )
            {
            // InternalXhtml.g:1659:2: ( ( rule__XmlStyle__AttributeAlternatives_0_0 ) )
            // InternalXhtml.g:1660:3: ( rule__XmlStyle__AttributeAlternatives_0_0 )
            {
             before(grammarAccess.getXmlStyleAccess().getAttributeAlternatives_0_0()); 
            // InternalXhtml.g:1661:3: ( rule__XmlStyle__AttributeAlternatives_0_0 )
            // InternalXhtml.g:1661:4: rule__XmlStyle__AttributeAlternatives_0_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlStyle__AttributeAlternatives_0_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlStyleAccess().getAttributeAlternatives_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__AttributeAssignment_0"


    // $ANTLR start "rule__XmlStyle__ValueAssignment_2"
    // InternalXhtml.g:1669:1: rule__XmlStyle__ValueAssignment_2 : ( ( rule__XmlStyle__ValueAlternatives_2_0 ) ) ;
    public final void rule__XmlStyle__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalXhtml.g:1673:1: ( ( ( rule__XmlStyle__ValueAlternatives_2_0 ) ) )
            // InternalXhtml.g:1674:2: ( ( rule__XmlStyle__ValueAlternatives_2_0 ) )
            {
            // InternalXhtml.g:1674:2: ( ( rule__XmlStyle__ValueAlternatives_2_0 ) )
            // InternalXhtml.g:1675:3: ( rule__XmlStyle__ValueAlternatives_2_0 )
            {
             before(grammarAccess.getXmlStyleAccess().getValueAlternatives_2_0()); 
            // InternalXhtml.g:1676:3: ( rule__XmlStyle__ValueAlternatives_2_0 )
            // InternalXhtml.g:1676:4: rule__XmlStyle__ValueAlternatives_2_0
            {
            pushFollow(FOLLOW_2);
            rule__XmlStyle__ValueAlternatives_2_0();

            state._fsp--;


            }

             after(grammarAccess.getXmlStyleAccess().getValueAlternatives_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XmlStyle__ValueAssignment_2"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000012000072L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000C800030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000013000070L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000060000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000003FE030L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x00000000007FE000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000000007FE002L});

}