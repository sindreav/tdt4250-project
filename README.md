# TDT4250-project
A project for the course TDT4250, made by Morten Bujordet and Sindre Vingen.

# Task
Find a kind of artifact, e.g. file format, preferably text-based, that is used within software development. Create an alternative textual syntax that is easier to use (read and write) and implement an Xtext-based editor for it, and an M2M or M2T transformation to the existing format.

# Info
The result of this project is a DSL that makes it easier to read and write HTML files. The DSL itself is name xHtml.

![alt text](https://gitlab.stud.idi.ntnu.no/sindreav/tdt4250-project/raw/master/images/xtext.png)

Clone the project and open it with Eclipse. When trying to clone it ourselves we got 11 errors where 6 of them were missing plugins. Although the project worked fine without these plugins installed.

Test files (.xhtml, .html and .xmi) are provided in the tests folder within tdt4250.xhtml.xhtml2XMI project. The main project for the DSL is tdt4250.xhtml.
To test the DSL please run a new instance of Eclipse with the xhtml-plugin integrated.
When the new instance is ready, make a new Java project and then make a new File with the extension .xhtml within the src folder using Eclipse. You will be prompted with a option to convert the project into an Xtext project, and you NEED to select 'YES'. In section More Info in this README file will tell you more about the syntax and how to use the DSL, but as for now just write the code below inside your .xhtml file:
```
div #id .class name="name" "background-color":red(
    /s color:white(text)
)
```
If you now save the .xhtml file a new folder "src-gen" is created and a .html file is placed inside this folder.

It is also made a M2M transformation program called xhtml2XMI (tdt4250.xhtml.xhtml2XMI). Run the Xhtml2XMI.java file in the src folder as a normal Java Application inside Eclipse. By providing arguments you tell the program where the input file is and what the output file should be named. Example is shown below.

![alt text](https://gitlab.stud.idi.ntnu.no/sindreav/tdt4250-project/raw/master/images/xmiMaker.png)

The input and output files should be located within the xhtml2XMI project, for instance in the tests folder.
The first argument represents the input file. For instance: tests/test1.xhtml (needs to be a .xtml file, and this argument is required)
The second arugment represents the output file. For instance: tests/test1.xmi (needs to be a .xmi file and this argument is optional)


# More Info
This language supports comments with "%" and variables, but also most common attributes are easier to write. As shown in the image above ids can be written #ID and classes .CLASS, attributes are written name=value and styles name:value.

```
    div()                           => <div/>
    /d()                            => <div/>
    /d #div1()                      => <div id="div1"/>
    /d .divs()                      => <div class="divs"/>
    /d name="theDiv"()              => <div name="theDiv"/>
    /d "background-color":"#000"()  => <div style="background-color:#000"/>
    /d (/s())                       => <div><span/></div>
    /s (TEXT)                       => <span>TEXT</span>
    % Comment here %                => <!-- Comment here -->
```

There are also some shortcuts for the most common HTML elements like div and span. These shortcuts are written with a slash in front and the tabel below shows what the shortcuts are.

| Shortcut | Represents |
| --- | --- |
| /d | div |
| /s | span |
| /h | head |
| /b | body |
| /t | title |
| /sc | script |

The html-tags are automatically inserted before and after the generated html file.

To test the DSL please run a new instance of Eclipse with the xhtml-plugin integrated. Then make a file with the extension .xhtml, write, save and a new .html file should be generated.

For the validation there are added 2 checks; if elements containing the same id and if a variable is used before it is initialized. The checks are found in the file XhtmlValidator.xtend inside the src folder in the tdt4250.xhtml project.
