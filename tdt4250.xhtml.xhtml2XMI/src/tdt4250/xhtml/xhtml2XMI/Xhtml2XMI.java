package tdt4250.xhtml.xhtml2XMI;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream.GetField;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Injector;

import tdt4250.XhtmlStandaloneSetup;
import tdt4250.xhtml.Domainmodel;

public class Xhtml2XMI {

	public static void main(String[] args) throws Exception {
		try {
		String input = null;
		String output = null;
		String inputFileName = null;
		
		if(args.length >= 1) input = args[0];
		if(args.length >= 2) output = args[1];
		
		if(input == null) throw new Exception("No input file path is given.");
		String extension = getExtension(input);
		if(!extension.equals("xhtml")) throw new Exception("Input file is not an .xhtml-file.");
		if(output == null) output = inputFileName+".xmi";
		extension = getExtension(output);
		if(!extension.equals("xmi")) throw new Exception("Output file is not an .xmi-file.");

		File file = new File(input);
		inputFileName = file.getName().split("\\.")[0];
	
		Injector injector = new XhtmlStandaloneSetup().createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		
		URI uri = URI.createURI(input);
		Resource xTextResource = resourceSet.getResource(uri, true);
		    
		EcoreUtil.resolveAll(xTextResource);
		    
		Resource xmiResource = resourceSet.createResource(URI.createURI(output));
		xmiResource.getContents().add(xTextResource.getContents().get(0));
		
		    xmiResource.save(null);
		}catch (Exception e) {
			throw new Exception("Could not convert file to XMI. "+e.toString(), e);
		} 
	}
	
	private static String getExtension(String s) {
		String r = "";
		int i = s.lastIndexOf('.');
		if (i > 0) {
		    r = s.substring(i+1);
		}
		return r;
	}
	
}
