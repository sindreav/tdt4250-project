/**
 * generated by Xtext 2.15.0
 */
package tdt4250.ui.contentassist;

import tdt4250.ui.contentassist.AbstractXhtmlProposalProvider;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class XhtmlProposalProvider extends AbstractXhtmlProposalProvider {
}
