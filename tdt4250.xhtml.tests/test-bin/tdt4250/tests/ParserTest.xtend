package tdt4250.tests

import org.junit.runner.RunWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.util.ParseHelper
import javax.inject.Inject
import org.junit.Test
import org.eclipse.xtext.util.EmfFormatter
import tdt4250.xhtml.Domainmodel
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.eclipse.xtext.generator.InMemoryFileSystemAccess
import tdt4250.generator.XhtmlGenerator

@RunWith(XtextRunner)
@InjectWith(XhtmlInjectorProvider)
class ParserTest {
	@Inject ParseHelper<Domainmodel> parser
	@Inject ValidationTestHelper validator;
	@Inject XhtmlGenerator generator;
	
	@Test def void testParser(){
		val model = parser.parse('/d #id .class name="name" background:red()');
		println(EmfFormatter.objToStr(model));
		validator.assertNoErrors(model);
		val fsa = new InMemoryFileSystemAccess;
		generator.doGenerate(model.eResource, fsa, null);
	}
}