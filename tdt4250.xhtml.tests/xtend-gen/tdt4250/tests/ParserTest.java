package tdt4250.tests;

import javax.inject.Inject;
import org.eclipse.xtext.generator.InMemoryFileSystemAccess;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.util.EmfFormatter;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Test;
import org.junit.runner.RunWith;
import tdt4250.generator.XhtmlGenerator;
import tdt4250.tests.XhtmlInjectorProvider;
import tdt4250.xhtml.Domainmodel;

@RunWith(XtextRunner.class)
@InjectWith(XhtmlInjectorProvider.class)
@SuppressWarnings("all")
public class ParserTest {
  @Inject
  private ParseHelper<Domainmodel> parser;
  
  @Inject
  private ValidationTestHelper validator;
  
  @Inject
  private XhtmlGenerator generator;
  
  @Test
  public void testParser() {
    try {
      final Domainmodel model = this.parser.parse("/d #id .class name=\"name\" background:red()");
      InputOutput.<String>println(EmfFormatter.objToStr(model));
      this.validator.assertNoErrors(model);
      final InMemoryFileSystemAccess fsa = new InMemoryFileSystemAccess();
      this.generator.doGenerate(model.eResource(), fsa, null);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
